class Http_subscribeTicker : public http_resource {

	const http_response render(const http_request& req){
		string ip = req.get_arg("ip");
		string username = req.get_arg("username");
		string password = req.get_arg("password");
		string license = req.get_arg("license");
		string appId = req.get_arg("appId");
		string portstr = req.get_arg("port");
		string prodCode = req.get_arg("prodCode");
		string str;
		bool error = FALSE;
		check(&str, "ip", ip, &error);
		check(&str, "username", username, &error);
		check(&str, "password", password, &error);
		check(&str, "license", license, &error);
		check(&str, "appId", appId, &error);
		check(&str, "port", portstr, &error);
		check(&str, "prodCode", prodCode, &error);

		ApiAccount apiAccount(ip, username, password, license, appId);

		if (!error) {
			LOGIN
					
			cout << "subscribe ticker " << prodCode << endl;
			apiTester->SubscribeTickerSync((char *) username.c_str(), (char *) prodCode.c_str());
			str = "subscribe ticker " + prodCode + " ok";

			Driver *driver;
			Connection *con;
			PreparedStatement *prep_stmt;
			try {
				driver = get_driver_instance();
				con = driver->connect("tcp://127.0.0.1", "sp", "sp");
				con->setSchema("sp");
				prep_stmt = con->prepareStatement("select count(*) from subscribeTicker where ip=? and username=? and password=? and license=? and appId=? and prodCode=?;");
				string s = apiTester->host + string(":") + to_string(apiTester->port);
				prep_stmt->setString(1, s.c_str());
				prep_stmt->setString(2, apiTester->user_id);
				prep_stmt->setString(3, apiTester->password);
				prep_stmt->setString(4, apiTester->license);
				prep_stmt->setString(5, apiTester->app_id);
				prep_stmt->setString(6, prodCode.c_str());
				ResultSet *sqlRes = prep_stmt->executeQuery();
				sqlRes->next();
				int count = sqlRes->getInt("count(*)");
				delete sqlRes;
				delete prep_stmt;
				cout << "cccc=" << count << endl;
				if (count == 0) {
					prep_stmt = con->prepareStatement("insert into subscribeTicker values(0, ?, ?, ?, ?, ?, ?);");
					string s = apiTester->host + string(":") + to_string(apiTester->port);
					prep_stmt->setString(1, apiTester->user_id);
					prep_stmt->setString(2, apiTester->password);
					prep_stmt->setString(3, s.c_str());
					prep_stmt->setString(4, apiTester->license);
					prep_stmt->setString(5, apiTester->app_id);
					prep_stmt->setString(6, prodCode.c_str());
					prep_stmt->execute();
					delete prep_stmt;
				}
			} catch (SQLException &e) {
				cout << "# ERR: SQLException in " << __FILE__;
				cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
				cout << "# ERR: " << e.what();
				cout << " (MySQL error code: " << e.getErrorCode();
				cout << ", SQLState: " << e.getSQLState() << " )" << endl;
			}
			delete con;
		} else {
			return http_response_builder("error, wrong parameter", 500).string_response();
		}

		cout << "------------------------------------------------------------------" << endl;
		return http_response_builder(str, 200).string_response();
	}
};
