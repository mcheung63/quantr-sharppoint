CC = g++
CFLAGS = -g -c -O3 -std=c++0x -fexceptions -frtti -w
LD = $(CC)
LDFLAGS = 
CPPFLAGS = -g -D__LINUX__=1 -DBOOST_MODE=1 -DBOOST_UTILS=1 -D_DEBUG=1 #-I/root/Downloads/mysql-connector-c++-1.1.7-linux-glibc2.5-x86-64bit/include

CWD :=  $(shell readlink -en $(dir $(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST))))

LIB_PATH := -L$(CWD)/lib
LIB_PATH += -L/usr/local/include/
#LIB_PATH += -L/root/Downloads/mysql-connector-c++-1.1.7-linux-glibc2.5-x86-64bit/lib
LIB := -lapiwrapper
LIB += -lhttpserver
LIB += -lmysqlcppconn
LIB += -lcurlpp
LIB += -lcurl

INC := -I./
INC += -I$(CWD)/include
#INC += -I$(CWD)/restbed/distribution/include

MODULE = sp

rwildcard=$(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2) $(filter $(subst *,%,$2),$d))

CPP_FILES = $(call rwildcard, , ApiTester.cpp sp.cpp helper.cpp ApiAccount.cpp)
OBJECTS = $(patsubst %.cpp, %.o, $(CPP_FILES))

all: $(MODULE)

$(MODULE): $(OBJECTS)
	$(LD) $(INC) $(LDFLAGS) $(LIB_PATH) -o $@ $^ $(LIB)

%.o: %.cpp
	$(CC) $(INC) $(CFLAGS) $(CPPFLAGS) $^ -o $@

clean:
	rm -f exit$(TARGET) $(OBJECTS) $(MODULE)

run:
	LD_LIBRARY_PATH=lib/:/usr/local/lib:/root/Downloads/mysql-connector-c++-1.1.7-linux-glibc2.5-x86-64bit/lib ./sp
