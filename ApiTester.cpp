#include "ApiTester.h"
#include <unistd.h>
#include <ctype.h>
#include <time.h> 
#include <stdio.h>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>
#include <sstream>

#include "helper.h"

using namespace curlpp::options;

ApiTester::ApiTester(void) {
}

ApiTester::~ApiTester(void) {
}

void ApiTester::Init() {
}

long EncodeDateTimeSerial(u_short year, u_short month, u_short day, u_short hour, u_short min, u_short sec) {
	struct tm tm;

	memset(&tm, 0, sizeof (tm));
	tm.tm_year = year - 1900;
	tm.tm_mon = month - 1;
	tm.tm_mday = day;
	tm.tm_hour = hour;
	tm.tm_min = min;
	tm.tm_sec = sec;
	return mktime(&tm);
}

char *ConnectionStatus(int status) {
	switch (status) {
		case 1: return"NOT_LOGIN";
		case 2: return"IN_PROGRESS";
		case 3: return"DONE";
		case 4: return"FAILED";
		case 5: return"LOGOUT";
		case 6: return"API_BLOCKED";
	}
	return "???";
}

char *OutputOrderStatus(char status) {
	switch (status) {
		case ORDSTAT_SENDING: return"sending";
		case ORDSTAT_WORKING: return"working";
		case ORDSTAT_INACTIVE: return"inactive";
		case ORDSTAT_PENDING: return"pending";
		case ORDSTAT_ADDING: return"adding";
		case ORDSTAT_CHANGING: return"changing";
		case ORDSTAT_DELETING: return"deleting";
		case ORDSTAT_INACTING: return"inacting";
		case ORDSTAT_PARTTRD_WRK: return"parttrd_wrk";
		case ORDSTAT_TRADED: return"traded";
		case ORDSTAT_DELETED: return"deleted";
		case ORDSTAT_APPROVEWAIT: return"approve wait";
		case ORDSTAT_TRADEDREP: return"traded & reported";
		case ORDSTAT_DELETEDREP: return"deleted & reported";
		case ORDSTAT_RESYNC_ABN: return"resync abnormal";
		case ORDSTAT_PARTTRD_DEL: return"partial traded & deleted";
		case ORDSTAT_PARTTRD_REP: return"partial traded & reported (deleted)";
	}
	return "???";
}

void ApiTester::HelpInfo() {
	int rc;
	char ver_no[100], rel_no[100], suffix[100];
	rc = apiProxyWrapper.SPAPI_GetDllVersion(ver_no, rel_no, suffix);
	printf("|------------------------------------Help------------------------------------|");
	printf("\n|   DLL Ver: %s                                                        |", ver_no);
	printf("\n|   Rel Ver: %s                                                        |", rel_no);
	printf("\n|   Dll Dat: %s                                                        |", suffix);
	printf("\n|----------------------------------------------------------------------------|");
	printf("\n| q:AddOrder        Q:SendMMOrder      w:DeleteOrder       e:ChangeOrder     |");
	printf("\n| r:GetActiveOrders t:SetOrderInactive y:GetOrderCount                       |");
	printf("\n| u:PosCount        i:GetPosByProduct  o:TradeCount        p:GetAllTrade     |");
	printf("\n|----------------------------------------------------------------------------|");
	printf("\n| a:SubPrice        s:UninitPrice      d:GetPriceByCode                      |");
	printf("\n| f:SubUnQuote      g:SubUnAllQuote    h:SubTicker         j:UninitTicker    |");
	printf("\n| k:LoadProductBy   l:GetProduct       :GetProductByCode   ':GetProductCount |");
	printf("\n| \\:LoadInstList    /:GetInstrument    .:GetInstByCode     ,:GetInstCount    |");
	printf("\n|----------------------------------------------------------------------------|");
	printf("\n| z:GetAccInfo      x:GetAccBalCount   c:GetAllAccBal                        |");
	printf("\n| v:GetAccBalByCurrency                b:GetConnStatus     n:GetCcyRateByCcy |");
	printf("\n| -:ChangePassword  =:SetApiLogPath    [:AccountLogin      ]:AccountLogout   |");
	printf("\n|----------------------------------------------------------------------------|");
	printf("\n|   1:Login            9:Help                                                |");
	printf("\n|----------------------------------------------------------------------------|");
}

int ApiTester::Login() {

	int rc, result, langid;

	//	printf("\n:_______________________________________________");
	//	printf("\n|======           SP API Login            ======|");
	//	printf("\n|_______________________________________________|");

	/*printf("\nSet API Language Id:[0:ENG, 1:Traditional Chinese, 2:Simplified Chinese]>>> ");
	scanf("%d", &langid);
	 */
	apiProxyWrapper.SPAPI_SetLanguageId(langid);
	apiProxyWrapper.SPAPI_Initialize();
	apiProxyWrapper.SPAPI_RegisterApiProxyWrapperReply(this);
	/*
			printf("\n| [1] Host : ");
			scanf("%s", host);
			printf("\n| [2] Port [AE:8081 Client:8080]: ");
			scanf("%hd", &port);
			printf("\n| [3] User Id : ");
			scanf("%s", user_id);
			printf("\n| [4] Password : ");
			scanf("%s", password);
			printf("\n| [5] License	: ");
			scanf("%s", license);
			printf("\n| [6] App Id : ");
			scanf("%s", app_id);*/

	printf("host=%s:%d\n", host, port);
	printf("license=%s\n", license);
	printf("app_id=%s\n", app_id);
	printf("user_id=%s\n", user_id);
	printf("password=%s\n", password);
	apiProxyWrapper.SPAPI_SetLoginInfo(host, port, license, app_id, user_id, password);
	rc = apiProxyWrapper.SPAPI_Login();
	
	printf("SPAPI_Login rc=%d\n", rc);
	return rc;
}

int ApiTester::LoginSync() {
	int rc = Login();
	if (rc != 0) {
		return 0;
	}
	while (logined == -1) {
		printf("waiting logined!=-1, sleeping\n");
		usleep(250000);
	}
	return logined;
}

void ApiTester::AddOrder() {
	SPApiOrder ord;
	int rc, cond, t;
	int dec_price;
	double stop, toler, step, level, profit, loss, loss_tol, mkt_prc;

	int pc, set_mkt_price;
	SPApiPrice price;
	double mkt_toler;
	set_mkt_price = 0;
	int is_inactiveOrder = 0; //0:��Ч��1:��Ч

	memset(&ord, 0, sizeof (ord));
	memset(&price, 0, sizeof (price));
	printf("\n|----------Input Add Order Cond----------|");
	printf("\n|   1:Normal,         2:Enhanced Stop,   |");
	printf("\n|   3:OCO,            4:Bull & Bear,     |");
	printf("\n|   5:Time to Send,   6:Open&Close(Today)|");
	printf("\n|----------------------------------------|");
	scanf("%d", &cond);
	if (cond < 1 || cond > 6) {
		printf("\n>>>>>>>>>>Input Add Order Cond Error!");
		return;
	}

	printf("\n>>>>>>>>Input User Id: ");
	scanf("%s", &ord.Initiator);
	getchar();
	printf("\n>>>>>>>>Input Acc No: ");
	scanf("%s", &ord.AccNo);
	getchar();


	strcpy(ord.Ref, "@API3_VC");
	strcpy(ord.Ref2, "");
	strcpy(ord.GatewayCode, "");
	ord.ValidType = 0;
	ord.CondType = 0;
	ord.OpenClose = OC_DEFAULT;

	printf("\n>>>>>>>>Input Order ProdCode: ");
	scanf("%s", &ord.ProdCode);
	getchar();
	printf("\n>>>>>>>>Input ProdCode DecInPrice: ");
	scanf("%d", &dec_price);
	printf("\n>>>>>>>>Set T+1 (1:Yes,0:No)");
	scanf("%d", &t);
	printf("\n>>>>>>>>Input ClOrderId:");
	scanf("%s", &ord.ClOrderId);
	ord.DecInPrice = dec_price;
	if (t == 1) ord.OrderOptions = 1;
	/*�@��*//*һ��*/
	if (cond == 1) {
		ord.CondType = 0;
		int price_flag, validity_type;
		printf("\n>>>>>>>>Price Type(0:Limit,1:AO,2:Market Price): ");
		scanf("%d", &price_flag);
		if (price_flag == 0) {
			printf("\n>>>>>>>>Input Order Price: ");
			scanf("%lf", &ord.Price);
			ord.OrderType = ORD_LIMIT;
		}
		if (price_flag == 1) {
			/*AO_PRC =����  �v��*/
			ord.Price = AO_PRC;
			ord.OrderType = ORD_AUCTION;
			ord.StopType = 0;
			ord.StopLevel = 0;
		}
		if (price_flag == 2) {
			/*�м۵� ������*/
			ord.Price = 0;
			ord.OrderType = ORD_MARKET;
			ord.StopType = 0;
			ord.StopLevel = 0;
		}

		if (price_flag == 0) {
			/*������Ч������ �q�榳�Ĵ�����*/
			printf("\n>>>>>>>>Input Validity Type(0:ToDay,1:FaK,2:FoK,3:GTC,4:GTD): ");
			scanf("%d", &validity_type);
			ord.ValidType = validity_type;
			if (validity_type == 4) {
				/*4:GTD ָ�����ڶ���  ���w�����q��*/
				int year, month, day;
				printf("\n>>>>>>>>Input Date(form:yyyy-mm-dd): ");
				scanf("%d-%d-%d", &year, &month, &day);
				ord.ValidTime = EncodeDateTimeSerial(year, month, day, 23, 59, 59);
			}

			if (validity_type == 0 || validity_type == 3 || validity_type == 4) {
				/*����Ч������Ϊ 0:ToDay,3:GTC,4:GTDʱ,�û������� ֹ��/���� �۸�*/
				/*�����Ĵ������� 0:ToDay,3:GTC,4:GTD��,�Τ��i�]�m ���l/Ĳ�o ����*/
				int stop_trigger, flag;
				printf("\n>>>>>>>>Input Stop/Trigger Price(1:Yes,0:No): ");
				scanf("%d", &flag);
				if (flag == 1) {
					ord.CondType = 1;
					/*ֹ���봥���۸�������,0:�޼�ֹ��,1:���д���,2:Ծ�д���,3:�г�ֹ��*/
					/*���l�PĲ�o���檺����,0:�������l,1:�ɥ�Ĳ�o,2:�D��Ĳ�o,3:�������l*/
					printf("\n>>>>>>>>Input Stop/Trigger Type(0:Stop Limit,1:UpTrigger,2:DownTrigger,3:Stop Market): ");
					scanf("%d", &stop_trigger);
					if (stop_trigger == 0)ord.StopType = STOP_LOSS;
					if (stop_trigger == 1)ord.StopType = STOP_UP;
					if (stop_trigger == 2)ord.StopType = STOP_DOWN;
					if (stop_trigger == 3) {
						ord.Price = 0;
						ord.OrderType = ORD_MARKET;
						ord.StopType = STOP_LOSS;
					}
					printf("\n>>>>>>>>Input Stop Price:");
					scanf("%lf", &ord.StopLevel);
				}
			}

		}


		printf("\n>>>>>>>>Input Order Qty: ");
		scanf("%d", &ord.Qty);
		getchar();
		printf("\n>>>>>>>>Input Order Buy(B) or Sell(S): ");
		scanf("%c", &ord.BuySell);
		printf("\n>>>>>>>>Activate or Inactive Order[0:Activate, 1:Inactive]:");
		scanf("%d", &is_inactiveOrder);
	}

	/*�W�j���l*//*��ǿֹ��*/
	if (cond == 2) {
		ord.CondType = 6;
		printf("\n>>>>>>>>Input Order Market Price: ");
		scanf("%lf", &mkt_prc);
		printf("\n>>>>>>>>Input Order Level Price: ");
		scanf("%lf", &level);
		printf("\n>>>>>>>>Input Order Toler: ");
		scanf("%lf", &toler);
		printf("\n>>>>>>>>Input Order step: ");
		scanf("%lf", &step);
		printf("\n>>>>>>>>Input Order Qty: ");
		scanf("%d", &ord.Qty);
		getchar();
		printf("\n>>>>>>>>Input Order Buy(B) or Sell(S): ");
		scanf("%c", &ord.BuySell);
		printf("\n>>>>>>>>Activate or Inactive Order[0:Activate, 1:Inactive]:");
		scanf("%d", &is_inactiveOrder);
		ord.StopLevel = level;
		ord.StopType = STOP_LOSS;

		if (ord.BuySell == 'B') {
			ord.UpLevel = mkt_prc;
			ord.UpPrice = level;
			ord.Price = level + toler;
			ord.DownLevel = step;
		} else {
			ord.DownLevel = mkt_prc;
			ord.DownPrice = level;
			ord.Price = level - toler;
			ord.UpLevel = step;
		}
	}


	/*���V�����L*//*˫���޼���*/
	if (cond == 3) {
		ord.CondType = 4;
		printf("\n>>>>>>>>Input Order Price: ");
		scanf("%lf", &ord.Price);
		printf("\n>>>>>>>>Input Order Qty: ");
		scanf("%d", &ord.Qty);
		getchar();
		printf("\n>>>>>>>>Input Order Buy(B) or Sell(S): ");
		scanf("%c", &ord.BuySell);
		printf("\n>>>>>>>>Input Order Stop: ");
		scanf("%lf", &stop);
		printf("\n>>>>>>>>Input Order Toler: ");
		scanf("%lf", &toler);
		printf("\n>>>>>>>>Activate or Inactive Order[0:Activate, 1:Inactive]:");
		scanf("%d", &is_inactiveOrder);
		if (ord.BuySell == 'B') {
			ord.UpLevel = stop;
			ord.UpPrice = stop + toler;
		} else {
			ord.DownLevel = stop;
			ord.DownPrice = stop - toler;
		}

	}

	/*�����P����*//*ţ��������*/
	if (cond == 4) {
		ord.CondType = 0;
		printf("\n>>>>>>>>Input Order Price: ");
		scanf("%lf", &ord.Price);
		printf("\n>>>>>>>>Input Order Profit: ");
		scanf("%lf", &profit);
		printf("\n>>>>>>>>Input Order Loss: ");
		scanf("%lf", &loss);
		printf("\n>>>>>>>>Input Order Loss Toler: ");
		scanf("%lf", &loss_tol);
		printf("\n>>>>>>>>Input Order Qty: ");
		scanf("%d", &ord.Qty);
		getchar();
		printf("\n>>>>>>>>Input Order Buy(B) or Sell(S): ");
		scanf("%c", &ord.BuySell);
		printf("\n>>>>>>>>Activate or Inactive Order[0:Activate, 1:Inactive]:");
		scanf("%d", &is_inactiveOrder);
		if (ord.BuySell == 'B') {
			ord.UpLevel = ord.Price + profit;
			ord.UpPrice = ord.Price + profit;
			ord.DownLevel = ord.Price - loss;
			ord.DownPrice = ord.Price - loss - loss_tol;
		} else {
			ord.DownLevel = ord.Price - profit;
			ord.DownPrice = ord.Price - profit;
			ord.UpLevel = ord.Price + loss;
			ord.UpPrice = ord.Price + loss + loss_tol;
		}
	}

	/*Ԥ��ʱ�䷢��*/
	if (cond == 5) {
		int year, month, day, hour, minute, second;
		printf("\n>>>>>>>>Input Date(form:yy-mm-dd): ");
		scanf("%d-%d-%d", &year, &month, &day);
		printf("\n>>>>>>>>Input Time(form:HH:mm:ss): ");
		scanf("%d:%d:%d", &hour, &minute, &second);
		ord.CondType = 3;
		ord.SchedTime = EncodeDateTimeSerial(year, month, day, hour, minute, second);
		printf("\n>>>>>>>>Input Order Price: ");
		scanf("%lf", &ord.Price);
		printf("\n>>>>>>>>Input Order Qty: ");
		scanf("%d", &ord.Qty);
		getchar();
		printf("\n>>>>>>>>Input Order Buy(B) or Sell(S): ");
		scanf("%c", &ord.BuySell);
		printf("\n>>>>>>>>Activate or Inactive Order[0:Activate, 1:Inactive]:");
		scanf("%d", &is_inactiveOrder);
	}

	/*����ƽ��ָ�� �}�ܥ��ܫ��O*/
	if (cond == 6) {
		int trigger, tri_type, cls_type, cond_type, step_idx;
		double profit, loss, loss_tol;
		double stop_loss, stop_loss_tol, trail_stop;
		int year, month, day, hour, minute, time_idx;

		ord.OrderType = 0;
		ord.OpenClose = OC_DEFAULT;
		ord.CondType = COND_COMBO_OPEN;
		ord.ValidType = VLD_REST_OF_DAY;

		printf("\n>>>>>>>>Input Open\n");
		printf("\n>>>>>>>>Set Open Trigger (1:Yes,0:No): ");
		scanf("%d", &trigger);

		if (trigger == 1) {
			/*�������ô������� �}�ܳ]�mĲ�o����*/
			printf("\n>>>>>>>>Input Open Trigger Type (1:UpTrigger,2:DownTrigger): ");
			scanf("%d", &tri_type);
			if (tri_type == 1) ord.StopType = STOP_UP;
			if (tri_type == 2) ord.StopType = STOP_DOWN;
			printf("\n>>>>>>>>Input Stop Level Price: ");
			scanf("%lf", &ord.StopLevel);
		} else {
			ord.StopType = 0;
			ord.StopLevel = 0;
		}

		printf("\n>>>>>>>>Input Order Price: ");
		scanf("%lf", &ord.Price);
		printf("\n>>>>>>>>Input Order Qty: ");
		scanf("%d", &ord.Qty);

		printf("\n>>>>>>>>Input Close\n");
		printf("\n>>>>>>>>Input Close Type (0:OCO(points), 1:Stop(points), 2:OCO(prices), 3:Stop(prices).): ");
		scanf("%d", &cls_type);
		/*ƽ������ ��������*/
		if (cls_type == 0 || cls_type == 2) {
			if (cls_type == 0) printf("\n>>>>>>>>Input Profit(points): ");
			else printf("\n>>>>>>>>Input Profit(prices): ");
			scanf("%lf", &profit);

			if (cls_type == 0) printf("\n>>>>>>>>Input Loss(points): ");
			else printf("\n>>>>>>>>Input Loss(prices): ");
			scanf("%lf", &loss);

			printf("\n>>>>>>>>Input Toler: ");
			scanf("%lf", &loss_tol);

			if (cls_type == 0) cond_type = COND_OCOSTOP;
			if (cls_type == 2) cond_type = COND_OCOSTOP_PRC;
			ord.UpLevel = cond_type;
			ord.UpPrice = profit;
			ord.DownLevel = loss;
			ord.DownPrice = loss_tol;
		} else if (cls_type == 1 || cls_type == 3) {
			if (cls_type == 1) printf("\n>>>>>>>>Input Profit(points): ");
			else printf("\n>>>>>>>>Input Profit(prices): ");
			scanf("%lf", &stop_loss);
			printf("\n>>>>>>>>Input Toler: ");
			scanf("%lf", &stop_loss_tol);
			printf("\n>>>>>>>>Input step(1:Yes,0:No): ");
			scanf("%d", &step_idx);
			if (step_idx == 1) {
				printf("\n>>>>>>>>Input Step: ");
				scanf("%lf", &trail_stop);
			} else
				trail_stop = 0;

			if (cls_type == 1) cond_type = (trail_stop == 0) ? COND_STOP : COND_TRAILSTOP;
			if (cls_type == 3) cond_type = (trail_stop == 0) ? COND_STOP_PRC : COND_TRAILSTOP_PRC;
			ord.UpLevel = cond_type;
			ord.UpPrice = trail_stop;
			ord.DownLevel = stop_loss;
			ord.DownPrice = stop_loss_tol;
		}

		if (trigger == 0 && (cls_type == 0 || cls_type == 1)) {
			printf("\n  market price order");
			//�м� //����
			/* pc = SPAPI_GetPriceByCode(ord.ProdCode, &price);
			 if (pc == 0)
			 {
					 printf("\n>>>>>>>>Set Market Price(1:Yes,0:No): ");scanf("%d",&set_mkt_price);
					 if(set_mkt_price == 1)
					 {
							printf("\n>>>>>>>>Input Market Toler(DecInPrice:%d): ", price.DecInPrice);scanf("%lf",&mkt_toler);
					 }
			 }*/
		}

		printf("\n>>>>>>>>Set Time to Close(1:Yes,0:No): ");
		scanf("%d", &time_idx);
		if (time_idx == 1) {
			/*ԤԼʱ��ƽ�� �w���ɶ�����*/
			printf("\n>>>>>>>>Input Date(form:yy-mm-dd): ");
			scanf("%d-%d-%d", &year, &month, &day);
			printf("\n>>>>>>>>Input Time(form:HH:mm): ");
			scanf("%d:%d", &hour, &minute);
			ord.SchedTime = EncodeDateTimeSerial(year, month, day, hour, minute, 0);
		}

		getchar();
		printf("\n>>>>>>>>Input Order Buy(B) or Sell(S): ");
		scanf("%c", &ord.BuySell);
		printf("\n>>>>>>>>Activate or Inactive Order[0:Activate, 1:Inactive]:");
		scanf("%d", &is_inactiveOrder);
		if (set_mkt_price == 1) {
			if (ord.BuySell == 'B')
				ord.Price = price.Ask[0] + mkt_toler;
			else
				ord.Price = price.Bid[0] - mkt_toler;
		}
	}

	for (int i = 0; i < ACount; i++)
		if (is_inactiveOrder == 0)rc = apiProxyWrapper.SPAPI_AddOrder(&ord);
		else apiProxyWrapper.SPAPI_AddInactiveOrder(&ord);

	printf("\n Added Order.");
	if (rc != 0) printf("Added Order Error:%d", rc);

}

int ApiTester::AddOrderSync(SPApiOrder ord) {
	int rc = apiProxyWrapper.SPAPI_AddOrder(&ord);
	return rc;
}

void ApiTester::SendMMOrder() {
	SPApiMMOrder mm_ord;
	int rc, t;
	int dec_price;
	char userId[16];

	memset(&mm_ord, 0, sizeof (mm_ord));

	GetUserId(userId);

	if (port == 8080) {
		strcpy(mm_ord.AccNo, acc_no);
	} else {
		printf("\n>>>>>>>>Input Acc No: ");
		scanf("%s", &mm_ord.AccNo);
		getchar();
	}
	mm_ord.ValidType = 0;
	mm_ord.OrderType = ORD_LIMIT;
	printf("\n>>>>>>>>Input Order ProdCode: ");
	scanf("%s", &mm_ord.ProdCode);
	getchar();
	printf("\n>>>>>>>>Input ProdCode DecInPrice: ");
	scanf("%d", &dec_price);

	printf("\n>>>>>>>>Input Bid Ext Order No: ");
	scanf("%lld", &mm_ord.BidExtOrderNo);
	printf("\n>>>>>>>>Input Bid Acc Order No: ");
	scanf("%ld", &mm_ord.BidAccOrderNo);
	printf("\n>>>>>>>>Input Bid Price: ");
	scanf("%lf", &mm_ord.BidPrice);
	printf("\n>>>>>>>>Input Bid Qty: ");
	scanf("%ld", &mm_ord.BidQty);
	printf("\n>>>>>>>>Input Ask Ext Order No: ");
	scanf("%lld", &mm_ord.AskExtOrderNo);
	printf("\n>>>>>>>>Input Ask Acc Order No: ");
	scanf("%ld", &mm_ord.AskAccOrderNo);
	printf("\n>>>>>>>>Input Ask Price: ");
	scanf("%lf", &mm_ord.AskPrice);
	printf("\n>>>>>>>>Input Ask Qty: ");
	scanf("%ld", &mm_ord.AskQty);

	printf("\n>>>>>>>>Set T+1 (1:Yes,0:No)");
	scanf("%d", &t);
	mm_ord.DecInPrice = dec_price;
	if (t == 1) mm_ord.OrderOptions = 1;


	/*
	int year,month,day, hour, minute, second;
	printf("\n>>>>>>>>Input Date(form:yy-mm-dd): ");  scanf("%d-%d-%d",&year,&month,&day);
	printf("\n>>>>>>>>Input Time(form:HH:mm:ss): ");  scanf("%d:%d:%d",&hour,&minute,&second);
	mm_ord.SpecTime = EncodeDateTimeSerial(year,month,day,hour,minute,second);
	 */

	rc = apiProxyWrapper.SPAPI_SendMarketMakingOrder(userId, &mm_ord);
	printf("\n Added Market Making Order.");
	if (rc != 0) printf(" Error:%d", rc);
}

int ApiTester::DeleteOrderSync(char userId[16], char accNo[16], char *prodCode, int orderNo) {
	int rc = apiProxyWrapper.SPAPI_DeleteOrderBy(userId, accNo, orderNo, prodCode, "");
	return rc;
}

void ApiTester::DeleteAllOrders() {
	char userId[16], accNo[16];
	int rc;

	GetUserId(userId);
	GetAccNo(accNo);
	rc = apiProxyWrapper.SPAPI_DeleteAllOrders(userId, accNo);
	if (rc != 0)printf("\n DeleteAllOrders, return : %d \n", rc);
}

int ApiTester::DeleteAllOrders(char userId[16], char accNo[16]) {
	int rc = apiProxyWrapper.SPAPI_DeleteAllOrders(userId, accNo);
	return rc;
}

//�޸Ĺ����еĶ���

void ApiTester::ChangeOrder() {
	long int_order_no;
	double new_price = 0;
	double chg_price = 0;
	SPApiOrder ord;
	int rc, oc, mode;
	char accNo[16];
	char userId[16];

	printf("\n>>>>>>>>[1:ChangeOrder, 2:ChangeOrderBy]: ");
	scanf("%d", &mode);
	if (mode == 1) {
		GetUserId(userId);
		GetAccNo(accNo);
		printf("\n>>>>>>>>Input Int Order No: ");
		scanf("%ld", &int_order_no);
		oc = apiProxyWrapper.SPAPI_GetOrderByOrderNo(userId, accNo, int_order_no, &ord);
		if (oc == 0) {
			printf("\n>>>>>>>>Input new_price: ");
			scanf("%lf", &new_price);
			printf("\n>>>>>>>>Input ClOrderId:");
			scanf("%s", &ord.ClOrderId);
			chg_price = new_price;
			new_price = ord.Price;
			ord.Price = chg_price;
			rc = apiProxyWrapper.SPAPI_ChangeOrder(userId, &ord, new_price, ord.Qty);
			if (rc != 0)printf("\n Change Order, return : %d \n", rc);
		} else
			printf("\n Get Order By Order No, return : %d \n", oc);
	} else if (mode == 2)ChangeOrderBy();
}

void ApiTester::ChangeOrderBy() {
	long int_order_no;
	double org_price, new_price;
	int org_qty, new_qty, rc;
	char userId[16], accNo[16];

	GetUserId(userId);
	GetAccNo(accNo);
	printf("\n>>>>>>>>Input Int Order No: ");
	scanf("%ld", &int_order_no);
	printf("\n>>>>>>>>Input org_price: ");
	scanf("%lf", &org_price);
	printf("\n>>>>>>>>Input new_price: ");
	scanf("%lf", &new_price);
	printf("\n>>>>>>>>Input org_qty: ");
	scanf("%d", &org_qty);
	printf("\n>>>>>>>>Input new_qty: ");
	scanf("%d", &new_qty);
	rc = apiProxyWrapper.SPAPI_ChangeOrderBy(userId, accNo, int_order_no, org_price, org_qty, new_price, new_qty);
	if (rc != 0)printf("\n Change Order By, return : %d \n", rc);
}

void ApiTester::SetOrderInactive() {
	long int_order_no, order_inactive;
	int rc, oc;
	SPApiOrder ord;
	char userId[16], accNo[16];
	printf("\n>>>>>>>>Input 1=Activate All, 2=Inactivate All, 3=Activate By No, 4=Inactivate By No:");
	scanf("%ld", &order_inactive);

	GetUserId(userId);
	GetAccNo(accNo);

	if (order_inactive == 1)
		rc = apiProxyWrapper.SPAPI_ActivateAllOrders(userId, accNo); //������ȫ������Ϊ��Ч����
	else if (order_inactive == 2)
		rc = apiProxyWrapper.SPAPI_InactivateAllOrders(userId, accNo); //������ȫ������Ϊ��Ч����
	else if (order_inactive == 3) {
		printf("\n>>>>>>>>Input Int Order No: ");
		scanf("%ld", &int_order_no);
		rc = apiProxyWrapper.SPAPI_ActivateOrderBy(userId, accNo, int_order_no); //��ָ���������ó���Ч
	} else if (order_inactive == 4) {
		printf("\n>>>>>>>>Input Int Order No: ");
		scanf("%ld", &int_order_no);
		rc = apiProxyWrapper.SPAPI_InactivateOrderBy(userId, accNo, int_order_no); //��ָ���������ó���Ч
	}
	if (rc != 0)printf("\n Activate or Inactivate [%d], return : %d \n", order_inactive, rc);
}

void ApiTester::GetActiveOrders() {
	vector<SPApiOrder> apiOrderList;
	char userId[16], accNo[16];
	int rc;

	GetUserId(userId);
	GetAccNo(accNo);
	rc = apiProxyWrapper.SPAPI_GetActiveOrders(userId, accNo, apiOrderList);
	if (rc != 0)printf("\n GetActiveOrders, return : %d \n", rc);
	for (int i = 0; i < apiOrderList.size(); i++) {
		SPApiOrder& order = apiOrderList[i];
		printf("\n[AccNo=%s], Order#=%u ,Status:%s, B/S = %c, ProdCode=%s, Price=%Lf, Qty=%ld, ClOrderId=%s, Initiator=%s", order.AccNo, order.IntOrderNo, OutputOrderStatus(order.Status), order.BuySell, order.ProdCode, order.Price, order.Qty, order.ClOrderId, order.Initiator);
	}
	if (apiOrderList.size() == 0) printf("\n No Order \n");
}

void ApiTester::GetOrderCount() {
	int count;
	char userId[16], accNo[16];

	GetUserId(userId);
	if (port == 8081)GetAccNo(accNo);
	else strcpy(accNo, user_id);
	count = apiProxyWrapper.SPAPI_GetOrderCount(userId, accNo);
	printf("\tAcc: %s, Order Count:%d\n", accNo, count);
}

int ApiTester::GetOrderCountSync(char userId[16], char accNo[16]) {
	int count = apiProxyWrapper.SPAPI_GetOrderCount(userId, accNo);
	printf("\tAcc: %s, Order Count:%d\n", accNo, count);
	return count;
}

int ApiTester::GetOrdersByArray(char* userId, char* acc_no, SPApiOrder* apiOrderList) {
	int r = apiProxyWrapper.SPAPI_GetOrdersByArray(userId, acc_no, apiOrderList);
	cout << "GetOrdersByArray, r=" << r << endl;
	return r;
}

void ApiTester::PosCount() {
	int count;

	char user[16];
	printf("\n|  User Id : ");
	scanf("%s", user);

	count = apiProxyWrapper.SPAPI_GetPosCount(user);
	printf("\n Acc: %s,Pos Count:%d", acc_no, count);
}

void ApiTester::GetPosByProduct() {
	int rc;
	SPApiPos pos;
	char prodcode[16];
	char user[16];

	GetUserId(user);
	memset(&pos, 0, sizeof (SPApiPos));
	printf("\n>>>>>>>>Input Pos ProdCode: ");
	scanf("%s", &prodcode);
	printf("\n>>>>>>>>Query:%s Position.", prodcode);
	rc = apiProxyWrapper.SPAPI_GetPosByProduct(user, prodcode, &pos);
	if (rc == 0)
		printf("\n ProdCode=%s,Prev.=%d @ %f,DayLong=%d @ %f,DayShort=%d @ %f, Rate:%f", pos.ProdCode, (pos.LongShort == 'B') ? pos.Qty : -1 * pos.Qty, pos.TotalAmt, pos.LongQty, pos.LongTotalAmt, pos.ShortQty, pos.ShortTotalAmt, pos.ExchangeRate);
	else
		printf("\n No Product");
}

void ApiTester::TradeCount() {
	int count;
	char userId[16], accNo[16];

	GetUserId(userId);
	GetAccNo(accNo);
	count = apiProxyWrapper.SPAPI_GetTradeCount(userId, accNo);
	printf("\n Acc: %s,Trade Count:%d", accNo, count);
}

void ApiTester::GetAllTrade() {
	vector<SPApiTrade> apiTradeList;
	char userId[16], accNo[16];
	int rc;

	GetUserId(userId);
	GetAccNo(accNo);
	rc = apiProxyWrapper.SPAPI_GetAllTrades(userId, accNo, apiTradeList);
	if (rc != 0)printf("\n GetAllTrades, return : %d \n", rc);
	for (int i = 0; i < apiTradeList.size(); i++) {
		SPApiTrade& trade = apiTradeList[i];
		printf("\nOrder#%d, TradeNo:%lld,ProdCode=%s , B/S=%c , Qty=%d, Price=%f, ", trade.IntOrderNo, trade.TradeNo, trade.ProdCode, trade.BuySell, trade.Qty, trade.Price);
	}
	if (apiTradeList.size() == 0) printf("\n No Trade \n");
}

vector<SPApiTrade> ApiTester::GetAllTradesSync(char* userId, char* acc_no) {
	vector<SPApiTrade> apiTradeList;
	//	char userId[16], accNo[16];
	int rc;

	//	GetUserId(userId);
	//	GetAccNo(accNo);
	rc = apiProxyWrapper.SPAPI_GetAllTrades(userId, acc_no, apiTradeList);
	//	if (rc != 0){
	//		printf("\n GetAllTrades, return : %d \n", rc);
	//	}
	//	for (int i = 0; i < apiTradeList.size(); i++) {
	//		SPApiTrade& trade = apiTradeList[i];
	//		printf("\nOrder#%d, TradeNo:%lld,ProdCode=%s , B/S=%c , Qty=%d, Price=%f, ", trade.IntOrderNo, trade.TradeNo, trade.ProdCode, trade.BuySell, trade.Qty, trade.Price);
	//	}
	//	if (apiTradeList.size() == 0){
	//		printf("\n No Trade \n");
	//	}

	return apiTradeList;
}

void ApiTester::SubscribeUninitPrice(int idx) {
	int rc;
	char prodcode[16], userId[16];

	GetUserId(userId);
	printf("\n>>>>>>>>Input Price ProdCode: ");
	scanf("%s", &prodcode);
	if (idx == 1)
		printf(">>>>>>>>Subscribe Price prodCode: %s\n", prodcode);
	else
		printf(">>>>>>>>Uninit Price prodCode: %s\n", prodcode);

	rc = apiProxyWrapper.SPAPI_SubscribePrice(userId, prodcode, idx); //idx, 0 :Uninit Price, 1:Subscribe Price, 2:set time to return to
	if (rc != 0)printf("\n SubscribeUninitPrice failed:%d\n", rc);
}

void ApiTester::SubscribeUninitPriceSync(int idx, char userId[16], char prodcode[16]) {
	int rc;
	if (idx == 1)
		printf("Subscribe Price prodCode: %s\n", prodcode);
	else
		printf("Uninit Price prodCode: %s\n", prodcode);

	rc = apiProxyWrapper.SPAPI_SubscribePrice(userId, prodcode, idx); //idx, 0 :Uninit Price, 1:Subscribe Price, 2:set time to return to
	if (rc != 0)printf("SubscribeUninitPrice failed:%d\n", rc);
}

void ApiTester::SubscribeQuoteRequest() {
	int rc, tc;
	char prodcode[16], userId[16];

	GetUserId(userId);
	printf("\nSubscribe = 1, Unsubscribe = 0. Input:");
	scanf("%d", &tc);
	if (tc < 0 || tc > 1) {
		printf("\n>>>>>>>>Input Quote Request[1:Subscribe, 0:Unsubscribe]");
		return;
	}
	printf("\n>>>>>>>>Input Quote Request ProdCode: ");
	scanf("%s", &prodcode);

	rc = apiProxyWrapper.SPAPI_SubscribeQuoteRequest(userId, prodcode, tc);
	printf("\n Subscribe or Un Quote Request return:%d", rc);
}

void ApiTester::SubscribeAllQuoteRequest() {
	int rc, tc;
	char prodcode[16], userId[16];

	GetUserId(userId);
	printf("\nSubscribe All Quote = 1, Unsubscribe All Quote = 0. Input:");
	scanf("%d", &tc);
	if (tc < 0 || tc > 1) {
		printf("\n>>>>>>>>Input Quote Request[1:Subscribe All, 0:Unsubscribe All]");
		return;
	}

	rc = apiProxyWrapper.SPAPI_SubscribeAllQuoteRequest(userId, tc);
	printf("\nSubscribe All or Un All Quote Request return:%d", rc);
}

void ApiTester::GetPriceByCode() {
	int rc;
	char prodcode[16], user_id[16];
	SPApiPrice price;

	printf("\n>>>>>>>>User Id:");
	scanf("%s", &user_id);
	printf("\n>>>>>>>>Input Price ProdCode: ");
	scanf("%s", &prodcode);

	printf("\n>>>>>>>>Price prodCode: %s", prodcode);
	rc = apiProxyWrapper.SPAPI_GetPriceByCode(user_id, prodcode, &price); //��֤��Լ������ �����õ��� //�O�ҦX���w�ӽ� �~�ள����
	if (rc == 0)
		printf("\n  ProdCode=%s , AskQty=%d , Ask=%f , Bid=%f , BidQty=%d  ", price.ProdCode, price.AskQty[0], price.Ask[0], price.Bid[0], price.BidQty[0]);
	else
		printf("\n No Subscribe Product:%d", rc);
}

void ApiTester::SubscribeTicker() {
	int rc;
	char prodcode[16], userId[16];

	GetUserId(userId);
	printf("\n>>>>>>>>Input Ticker ProdCode: ");
	scanf("%s", &prodcode);
	printf("\n>>>>>>>>Subscribe Ticker prodCode: %s", prodcode);
	rc = apiProxyWrapper.SPAPI_SubscribeTicker(user_id, prodcode, 1);
	if (rc != 0)printf("\nSubscribeTicker return:%d", rc);
}

void ApiTester::SubscribeTickerSync(char userId[16], char prodcode[16]) {
	int rc;
	rc = apiProxyWrapper.SPAPI_SubscribeTicker(user_id, prodcode, 1);
	if (rc == 0) {
		printf("SubscribeTicker return:%d\n", rc);
	} else {
		printf("SubscribeTicker failed:%d\n", rc);
	}
}

void ApiTester::UninitTicker() {
	int rc;
	char prodcode[16], userId[16];

	GetUserId(userId);
	printf("\n>>>>>>>>Input Uninit Ticker ProdCode: ");
	scanf("%s", &prodcode);
	printf("\n>>>>>>>>Uninit Ticker prodCode: %s", prodcode);
	rc = apiProxyWrapper.SPAPI_SubscribeTicker(userId, prodcode, 0);
	if (rc != 0)printf("\SubscribeTicker return:%d", rc);
}

void ApiTester::GetAccInfo() {
	SPApiAccInfo acc_info;
	int rc;
	char user[16];

	GetUserId(user);
	memset(&acc_info, 0, sizeof (SPApiAccInfo));
	rc = apiProxyWrapper.SPAPI_GetAccInfo(user, &acc_info);
	if (rc == 0) {
		//printf("\nAccInfo: acc_no: %s  AE:%s  BaseCcy:%s,  MarginClass:%s, NAV:%f, BuyingPower:%f, CashBal:%f, MarginCall:%f, CommodityPL:%Lf, LockupAmt:%f, Loan2MR:%f, Loan2MV:%f, AccName:%s", acc_info.ClientId, acc_info.AEId, acc_info.BaseCcy, acc_info.MarginClass, acc_info.NAV, acc_info.BuyingPower, acc_info.CashBal, acc_info.MarginCall, acc_info.CommodityPL, acc_info.LockupAmt, acc_info.LoanToMR, acc_info.LoanToMV, acc_info.AccName);
		//string str = Big2Gb(acc_info.AccName);
		//printf("\nAccInfo: AccName>>>Chinese simplified: %s", str.c_str());
	}

}

void ApiTester::GetAccBalCount() {
	int count;
	char user[16];

	GetUserId(user);
	count = apiProxyWrapper.SPAPI_GetAccBalCount(user);
	//printf("\n Acc: %s,Acc Bal Count:%d", user, count);
}

int ApiTester::GetAccBalCountSync(char userId[256]) {
	int count;
	//	char user[256];
	//	strcpy(user, userId);
	count = apiProxyWrapper.SPAPI_GetAccBalCount(userId);
	//printf("\n Acc: %s,Acc Bal Count:%d", userId, count);
	return count;
}

void ApiTester::GetAllAccBal() {
	char v_acc_no[16];
	vector<SPApiAccBal> apiAccBalList;
	char user[16];

	GetUserId(user);
	apiProxyWrapper.SPAPI_GetAllAccBal(user, apiAccBalList);
	usleep(5000000);
	for (int i = 0; i < apiAccBalList.size(); i++) {
		SPApiAccBal& accBal = apiAccBalList[i];
		printf("\n[No: %d, Acc No:%s]  Ccy=%s , CashBf=%f , NotYetValue=%f , TodayCash=%f , TodayOut=%f,  Unpresented=%f ", i + 1, user, accBal.Ccy, accBal.CashBf, accBal.NotYetValue, accBal.TodayCash, accBal.TodayOut, accBal.Unpresented);
	}
}

void ApiTester::GetAllAccBalSync(char accNo[16], vector<SPApiAccBal> *apiAccBalList) {
	apiProxyWrapper.SPAPI_GetAllAccBal(accNo, *apiAccBalList);
	for (int i = 0; i < apiAccBalList->size(); i++) {
		SPApiAccBal& accBal = (*apiAccBalList)[i];
		//printf("\n[PETER No: %d, Acc No:%s]  Ccy=%s , CashBf=%f , NotYetValue=%f , TodayCash=%f , TodayOut=%f,  Unpresented=%f ", i + 1, user_id, accBal.Ccy, accBal.CashBf, accBal.NotYetValue, accBal.TodayCash, accBal.TodayOut, accBal.Unpresented);
	}
}

void ApiTester::GetAccBalByCurrency() {
	SPApiAccBal accBal;
	int rc;
	char user[16];

	GetUserId(user);
	memset(&accBal, 0, sizeof (accBal));
	printf("\n>>>>>>>>Input Bal Ccy: ");
	scanf("%s", &accBal.Ccy);
	rc = apiProxyWrapper.SPAPI_GetAccBalByCurrency(user, accBal.Ccy, &accBal);
	if (rc == 0) {
		printf("\n  Ccy=%s , CashBf=%f , NotYetValue=%f , TodayCash=%f , TodayOut=%f,  Unpresented=%f ", accBal.Ccy, accBal.CashBf, accBal.NotYetValue, accBal.TodayCash, accBal.TodayOut, accBal.Unpresented);
	} else
		printf("\nFind No Ccy!");
}

void ApiTester::GetConnectionStatus() {
	int rc;
	int status_Id;
	char *host_name;
	char user_id[16];

	printf("\n |-----------------------------|");
	printf("\n |  80,81:Transaction Link     |");
	printf("\n |  83:Price Link              |");
	//printf("\n |  87:Long Depth Link         |");
	printf("\n |  88:Information Link        |");
	printf("\n |-----------------------------|");

	printf("\n>>>>>>>>User Id: ");
	scanf("%s", &user_id);
	printf("\n>>>>>>>>Input Status ID: ");
	scanf("%d", &status_Id);
	rc = apiProxyWrapper.SPAPI_GetLoginStatus(user_id, status_Id);

	switch (status_Id) {
		case 80:
			host_name = "Transaction Link";
			break;
		case 81:
			host_name = "Transaction Link";
			break;
		case 83:
			host_name = "Price Link";
			break;
		case 87:
			host_name = "Long Depth Link";
			break;
		case 88:
			host_name = "Information Link";
			break;
		default:
			host_name = "No Host Id";
	}
	printf("\nConn Status:Host id: %d,%s : %s[%d]", status_Id, host_name, ConnectionStatus(rc), rc);
}

void ApiTester::GetCcyRateByCcy() {
	int rc;
	double rate;
	char ccy_code[4];
	char user[16];

	GetUserId(user);
	printf("\n>>>>>>>>Input Ccy Code: ");
	scanf("%s", &ccy_code);
	printf("\n>>>>>>>>Query:%s Ccy.", ccy_code);
	rc = apiProxyWrapper.SPAPI_GetCcyRateByCcy(user, ccy_code, rate);
	if (rc == 0)
		printf("\n Ccy=%s, Rate:%f", ccy_code, rate);
	else
		printf("\n No Rate");
}

void ApiTester::ChangePassword() {
	int rc;
	char old_psw[16];
	char new_psw[16];
	char userId[16];

	GetUserId(userId);
	printf("\n>>>>>>>>Input old password: ");
	scanf("%s", &old_psw);
	printf("\n>>>>>>>>Input new password: ");
	scanf("%s", &new_psw);
	rc = apiProxyWrapper.SPAPI_ChangePassword(userId, old_psw, new_psw);
	if (rc != 0)printf("\n ChangePassword return:%d", rc);
}

void ApiTester::SetApiLogPath() {
	int rc;
	char path[500];

	printf("\n Set Log Path : ");
	scanf("%s", path);
	rc = apiProxyWrapper.SPAPI_SetApiLogPath(path);
	printf("\n[%s]Set Api Log Path Reply:%d ", acc_no, rc);
}

void ApiTester::AccountLogin() {
	int rc;
	char userId[16], accNo[16];

	GetUserId(userId);
	GetAccNo(accNo);

	//cout << "2 accNo=" << accNo << endl;
	//cout << "2 user_id=" << user_id << endl;
	rc = apiProxyWrapper.SPAPI_AccountLogin(userId, accNo);
	printf("\nAccount Login :[%s]%s, return:%d\n", userId, accNo, rc);
}

int ApiTester::AccountLoginSync(char accNo[16]) {
	int rc;
	rc = apiProxyWrapper.SPAPI_AccountLogin(user_id, accNo);
	//printf("\nAccount Login :[%s]%s, return:%d\n", user_id, accNo, rc);
	while (accountLogined == -1) {
		usleep(250000);
	}
	return rc;
}

void ApiTester::waitUntilLogin() {
	while (accountLogined == -1) {
		usleep(250000);
	}
}

void ApiTester::AccountLogout() {
	int rc;
	char userId[16], accNo[16];

	GetUserId(userId);
	GetAccNo(accNo);
	rc = apiProxyWrapper.SPAPI_AccountLogout(userId, accNo);
	printf("\nAccount Logout:[%s]%s, return:%d", userId, accNo, rc);
}

void ApiTester::LoadProductInfoListByCode() {
	int rc;
	char prodCode[16];
	printf("\n>>>>>>>>Input Instrument Code: ");
	scanf("%s", &prodCode);
	rc = apiProxyWrapper.SPAPI_LoadProductInfoListByCode(prodCode);
	printf("\nLoad Instrument Report Reply:%d ", rc);

}

void ApiTester::GetProductCount() {
	int count;

	count = apiProxyWrapper.SPAPI_GetProductCount();
	printf("\n Loaded Product Count:%d", count);
}

void ApiTester::GetProduct() {
	int rc;
	vector<SPApiProduct> apiProdList;

	apiProxyWrapper.SPAPI_GetProduct(apiProdList);
	for (int i = 0; i < apiProdList.size(); i++) {
		SPApiProduct& prod = apiProdList[i];
		printf("\n Number:%d  ProdCode=%s , ProdName=%s , InstCode=%s ", i + 1, prod.ProdCode, prod.ProdName, prod.InstCode);
	}
	printf("\n Product Count:%d", apiProdList.size());
}

void ApiTester::GetProductByCode() {
	int rc;
	char prodCode[16];
	SPApiProduct prod;

	printf("\n>>>>>>>>Input Product Code: ");
	scanf("%s", &prodCode);

	printf("\n>>>>>>>>Product Code: %s", prodCode);
	rc = apiProxyWrapper.SPAPI_GetProductByCode(prodCode, &prod);
	if (rc == 0)
		printf("\n ProdCode=%s , ProdName=%s , InstCode=%s ", prod.ProdCode, prod.ProdName, prod.InstCode);
}

void ApiTester::LoadInstrumentList() {
	int rc;

	rc = apiProxyWrapper.SPAPI_LoadInstrumentList();
	printf("\nLoad Instrument Report Reply:%d ", rc);
}

void ApiTester::GetInstrumentCount() {
	int count;

	count = apiProxyWrapper.SPAPI_GetInstrumentCount();
	printf("\n Instrument Count:%d", count);
}

void ApiTester::GetInstrument() {
	int rc;
	vector<SPApiInstrument> apiInstList;

	rc = apiProxyWrapper.SPAPI_GetInstrument(apiInstList);
	if (rc != 0)printf("\n Get Instrument, return : %d \n", rc);
	for (int i = 0; i < apiInstList.size(); i++) {
		SPApiInstrument& instrument = apiInstList[i];
		printf("\n Number:%d, InstCode=%s, InstName=%s, Ccy=%s, Margin=%f", i + 1, instrument.InstCode, instrument.InstName, instrument.Ccy, instrument.Margin);
	}
	if (apiInstList.size() == 0) printf("\n No Instrument \n");
}

void ApiTester::GetInstrumentByCode() {
	int rc;
	char instCode[16];
	SPApiInstrument instrument;

	printf("\n>>>>>>>>Input Instrument Code: ");
	scanf("%s", &instCode);

	printf("\n>>>>>>>>Instrument Code: %s", instCode);
	rc = apiProxyWrapper.SPAPI_GetInstrumentByCode(instCode, &instrument);
	if (rc == 0)
		printf("\n MarketCode=%s , InstCode=%s , InstName=%s , Ccy=%s , Margin=%lf  ", instrument.MarketCode, instrument.InstCode, instrument.InstName, instrument.Ccy, instrument.Margin);
}

void ApiTester::Unload() {
	//apiProxyWrapper.Unload();
}

void ApiTester::GetAccNo(char *acc) {
	char accNo[16];
	printf("\n|  Acc No : ");
	scanf("%s", accNo);
	strncpy(acc, accNo, 16);
}

void ApiTester::GetUserId(char *user) {
	char userId[16];

	printf("\n|  User Id : ");
	scanf("%s", userId);
	strncpy(user, userId, 16);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ApiTester::OnTest() {
	//cout << "Start.........................." << endl;
}

void ApiTester::OnLoginReply(long ret_code, char *ret_msg) {
	cout << "Login Reply returnCode = " << ret_code << endl;

	if (ret_code != 0) {
		cout << "Login ErrMsg = " << string(ret_msg) << ", ret_code=" << ret_code << endl;
		logined = ret_code;
		accountLogined = ret_code;
	} else {
		logined = 1;
		////cout << "Subscribe Price ESM6... " << endl;
		//apiProxyWrapper.SPAPI_SubscribePrice("LIN001", "ESM6", 1);
		////cout << "Subscribe Ticker ESM6... " << endl;
		//apiProxyWrapper.SPAPI_SubscribeTicker("LIN001", "ESM6", 1);
	}
}

void ApiTester::OnConnectedReply(long host_type, long con_status) {
	switch (host_type) {
		case 80:
		case 81:
			cout << "Host type :[" << host_type << "][" << con_status << "]Transaction... Please wait!" << endl;
			break;
		case 83:
			cout << "Host type :[" << host_type << "][" << con_status << "]Quote price port... Please wait" << endl;
			break;
		case 88:
			cout << "Host type :[" << host_type << "][" << con_status << "]Information Link... Please wait!" << endl;
			break;
	}

}

void ApiTester::OnApiOrderRequestFailed(tinyint action, const SPApiOrder *order, long err_code, char *err_msg) {
	cout << "Order Request Failed: Order#" << order->IntOrderNo << " [" << err_code << " (" + string(err_msg) << ")], Action=" << action << " ClorderId=" + string(order->ClOrderId) << endl;
}

void ApiTester::OnApiOrderReport(long rec_no, const SPApiOrder *order) {
	log("\t\t\tOrder Report [acc_no:" + string(order->AccNo) + "] Status=" + string(OutputOrderStatus(order->Status)) + " Order#" + to_string(order->IntOrderNo) + " ProdCode=" + string(order->ProdCode)
			+ "\tPrice=" + to_string(order->Price) + " Qty=" + to_string(order->Qty) + " TradedQty=" + to_string(order->TradedQty) + " TotalQty=" + to_string(order->TotalQty)
			+ "\tClOrderId=" + to_string(order->IntOrderNo));

	if (string(OutputOrderStatus(order->Status)) == "working") {
		SPApiOrder *newOrder = (SPApiOrder *) malloc(sizeof (SPApiOrder));
		memcpy(newOrder, order, sizeof (SPApiOrder));
		orders.push_back(newOrder);
		log("ADDED " + to_string(newOrder->IntOrderNo));
	} else if (string(OutputOrderStatus(order->Status)) == "deleted") {
		vector<int> removeOrderIds;
		for (int x = orders.size() - 1; x >= 0; x--) {
			SPApiOrder *oldOlder = orders[x];
			//cout<<oldOlder->AccNo<<" = "<<order->AccNo<<", "<<oldOlder->IntOrderNo<<" == "<<order->IntOrderNo<<endl;
			if (string(oldOlder->AccNo) == string(order->AccNo) && oldOlder->IntOrderNo == order->IntOrderNo) {
				removeOrderIds.push_back(x);
				log("DELETED " + string(order->AccNo) + " = " + to_string(order->IntOrderNo));
			}
		}
		for (int x = 0; x < removeOrderIds.size(); x++) {
			orders.erase(orders.begin() + removeOrderIds[x]);
		}
	} else if (string(OutputOrderStatus(order->Status)) == "traded") {
		double price = order->Price;
		log("notify server = " + string(restUrl + "/algo/tradeReport.htm?price=" + to_string(price)));

		try {
			curlpp::Cleanup myCleanup;
			curlpp::Easy myRequest;

			// Set the URL.
			CURL *curl = curl_easy_init();
			if (curl) {
				char *encodedRef = curl_easy_escape(curl, order->Ref, strlen(order->Ref));
				myRequest.setOpt<Url>(string(restUrl + "/algo/tradeReport.htm?price=" + to_string(price) + "&prodCode=" + string(order->ProdCode) + "&accNo=" + string(order->AccNo) + "&qty=" + to_string(order->TradedQty) + "&isBuy=" + to_string(order->BuySell) + "&ref=" + encodedRef + "&ref2=" + order->Ref2));
				myRequest.setOpt(new curlpp::options::Timeout(60));
				myRequest.setOpt(new curlpp::options::Verbose(true));

				// Send request and get a result.
				// By default the result goes to standard output.
				myRequest.perform();
			}
		} catch (curlpp::RuntimeError & e) {
			cout << e.what() << std::endl;
		} catch (curlpp::LogicError & e) {
			cout << e.what() << std::endl;
		}
		log("end notify server = " + string(restUrl + "/algo/tradeReport.htm?price=" + to_string(price)));
		//		ostringstream os;
		//		os << curlpp::options::Url(string(restUrl + "/algo/tradeReport.htm?price=" + to_string(price) + "&prodCode=" + string(order->ProdCode) + "&accNo=" + string(order->AccNo)));
		//		string result = os.str();
	}
}

void ApiTester::OnApiOrderBeforeSendReport(const SPApiOrder * order) {
	OnApiOrderReport(0, order);
}

void ApiTester::OnAccountLoginReply(char *accNo, long ret_code, char* ret_msg) {
	cout << "Account Login Reply: acc_no=" + string(accNo) << " ret_code=" << ret_code << " ret_msg=" + string(ret_msg) << endl;
}

void ApiTester::OnAccountLogoutReply(long ret_code, char* ret_msg) {
	cout << "Account Logout Reply:  ret_code=" << ret_code << " ret_msg=" + string(ret_msg) << endl;
}

void ApiTester::OnAccountInfoPush(const SPApiAccInfo * acc_info) {
	//cout << "AccInfo: acc_no=" + string(acc_info->ClientId) << " AE=" + string(acc_info->AEId) << " BaseCcy=" + string(acc_info->BaseCcy) << endl;
	accountLogined = 1;
}

void ApiTester::OnAccountPositionPush(const SPApiPos * pos) {
	int p_qty;
	if (pos->LongShort == 'B') {
		p_qty = pos->Qty;
	} else {
		p_qty = -1 * pos->Qty;
	}
	accountLogined = 1;
	cout << "Pos: ProdCode=" + string(pos->ProdCode) << " Prev=" << p_qty << "@" << pos->TotalAmt;
	cout << " DayLong=" << pos->LongQty << "@" << pos->LongTotalAmt;
	cout << " DayShort=" << pos->ShortQty << "@" << pos->ShortTotalAmt;
	cout << " PLBaseCcy=" << pos->PLBaseCcy << " PL=" << pos->PL << " ExcRate=" << pos->ExchangeRate << endl;
}

void ApiTester::OnUpdatedAccountPositionPush(const SPApiPos * pos) {
}

void ApiTester::OnUpdatedAccountBalancePush(const SPApiAccBal * acc_bal) {
	cout << "AccBal: Ccy=" + string(acc_bal->Ccy) << " CashBf=" << acc_bal->CashBf << " NotYetValue=" << acc_bal->NotYetValue << " TodayCash=" << acc_bal->TodayCash;
	cout << " TodayOut=" << acc_bal->TodayOut << " Unpresented=" << acc_bal->Unpresented << endl;
}

void ApiTester::OnApiTradeReport(long rec_no, const SPApiTrade * trade) {
	//	cout << "Trade Report: [RecNo="<<trade->RecNo<<" acc_no=" + string(trade->AccNo) << " Status=" << string(OutputOrderStatus(trade->Status)) << " ProdCode=" + string(trade->ProdCode);
	//	cout << " Order#: " << trade->IntOrderNo << " trade_no=" << trade->TradeNo << " OrderPrice=" << trade->OrderPrice << " Price=" << trade->Price << " avg_price=" << trade->AvgTradedPrice;
	//	cout << " trade_qty=" << trade->Qty << ", ExtOrderNo=" << trade->IntOrderNo << ", BuySell=" << trade->BuySell << endl;

}

void ApiTester::OnApiPriceUpdate(const SPApiPrice * price) {
	if (price == NULL)return;
	char buff[20];
	convertTime(buff, price->Timestamp);
	//cout << "\tPrice:" + string(price->ProdCode) << '\t' << price->BidQty[0] << '\t' << price->Bid[0] << '\t' << price->Ask[0] << '\t' << price->AskQty[0] << '\t' << buff << endl;
	priceHock(this, price);
}

void ApiTester::OnApiTickerUpdate(const SPApiTicker * ticker) {
	char buff[20];
	convertTime(buff, ticker->TickerTime);
	//cout << "\tTicker:" + string(ticker->ProdCode) << '\t' << ticker->Price << '\t' << ticker->Qty << "\t" << buff << endl;
	tickerHock(this, ticker);
}

void ApiTester::OnPswChangeReply(long ret_code, char *ret_msg) {
	cout << "Psw Change Reply:" << ret_code << '\t' + string(ret_msg) << endl;
}

void ApiTester::OnProductListByCodeReply(char *inst_code, bool is_ready, char *ret_msg) {
	printf("\nProductListByCodeReply(inst code:%s):%s. Ret Msg:%s\n", inst_code, is_ready ? "Ok" : "No", ret_msg);
}

void ApiTester::OnInstrumentListReply(bool is_ready, char *ret_msg) {
	printf("\nInstrument Ready:%s. Ret Msg:%s\n", is_ready ? "Ok" : "No", ret_msg);
}

void ApiTester::OnBusinessDateReply(long business_date) {
	cout << "Business Date: " << business_date << endl;
}

void ApiTester::OnApiMMOrderBeforeSendReport(SPApiMMOrder * mm_order) {
	printf("\nMM Order BeforeSend Report [acc_no:%s]:\nAskAccOrderNo:%ld , AskExtOrderNo#%lld , AskQty=%ld\nBidAccOrderNo:%ld , BidExtOrderNo#%lld, BidQty=%ld\n", mm_order->AccNo,
			mm_order->AskAccOrderNo, mm_order->AskExtOrderNo, mm_order->AskQty, mm_order->BidAccOrderNo, mm_order->BidExtOrderNo, mm_order->BidQty);
}

void ApiTester::OnApiMMOrderRequestFailed(SPApiMMOrder *mm_order, long err_code, char *err_msg) {
	printf("\nMM Order Request Failed:Order#%ld [%ld (%s)], ClorderId=%s", mm_order->AccNo, err_code, err_msg, mm_order->ClOrderId);
}

void ApiTester::OnApiQuoteRequestReceived(char *product_code, char buy_sell, long qty) {
	cout << "Quote Request: ProductCode:" + string(product_code) << "  b_s:" << buy_sell << " qty=" << qty << endl;
	//	(buy_sell == 0) strcpy(bs, "Both");
	//	(buy_sell == 'B')strcpy(bs, "Buy");
	//	(buy_sell == 'S')strcpy(bs, "Sell");
}

void convertTime(char buff[20], int32_t timeStamp) {
	memset(buff, 0, 20);
	struct tm *info;
	time_t rawtime = timeStamp;
	info = localtime(&rawtime);
	strftime(buff, 20, "%Y-%m-%d %H:%M:%S.%s", info);
}
