-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- 主機: localhost
-- 產生時間： 2016 年 09 月 05 日 00:19
-- 伺服器版本: 5.6.28-0ubuntu0.14.04.1
-- PHP 版本： 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `sp`
--

-- --------------------------------------------------------

--
-- 資料表結構 `price`
--

CREATE TABLE IF NOT EXISTS `price` (
  `id` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  `username` varchar(60) DEFAULT NULL,
  `ip` varchar(60) NOT NULL,
  `license` varchar(60) DEFAULT NULL,
  `appId` varchar(60) DEFAULT NULL,
  `Bid0` float DEFAULT NULL,
  `Bid1` float DEFAULT NULL,
  `Bid2` float DEFAULT NULL,
  `Bid3` float DEFAULT NULL,
  `Bid4` float DEFAULT NULL,
  `Bid5` float DEFAULT NULL,
  `Bid6` float DEFAULT NULL,
  `Bid7` float DEFAULT NULL,
  `Bid8` float DEFAULT NULL,
  `Bid9` float DEFAULT NULL,
  `Bid10` float DEFAULT NULL,
  `Bid11` float DEFAULT NULL,
  `Bid12` float DEFAULT NULL,
  `Bid13` float DEFAULT NULL,
  `Bid14` float DEFAULT NULL,
  `Bid15` float DEFAULT NULL,
  `Bid16` float DEFAULT NULL,
  `Bid17` float DEFAULT NULL,
  `Bid18` float DEFAULT NULL,
  `Bid19` float DEFAULT NULL,
  `BidQty0` float DEFAULT NULL,
  `BidQty1` float DEFAULT NULL,
  `BidQty2` float DEFAULT NULL,
  `BidQty3` float DEFAULT NULL,
  `BidQty4` float DEFAULT NULL,
  `BidQty5` float DEFAULT NULL,
  `BidQty6` float DEFAULT NULL,
  `BidQty7` float DEFAULT NULL,
  `BidQty8` float DEFAULT NULL,
  `BidQty9` float DEFAULT NULL,
  `BidQty10` float DEFAULT NULL,
  `BidQty11` float DEFAULT NULL,
  `BidQty12` float DEFAULT NULL,
  `BidQty13` float DEFAULT NULL,
  `BidQty14` float DEFAULT NULL,
  `BidQty15` float DEFAULT NULL,
  `BidQty16` float DEFAULT NULL,
  `BidQty17` float DEFAULT NULL,
  `BidQty18` float DEFAULT NULL,
  `BidQty19` float DEFAULT NULL,
  `BidTicket0` float DEFAULT NULL,
  `BidTicket1` float DEFAULT NULL,
  `BidTicket2` float DEFAULT NULL,
  `BidTicket3` float DEFAULT NULL,
  `BidTicket4` float DEFAULT NULL,
  `BidTicket5` float DEFAULT NULL,
  `BidTicket6` float DEFAULT NULL,
  `BidTicket7` float DEFAULT NULL,
  `BidTicket8` float DEFAULT NULL,
  `BidTicket9` float DEFAULT NULL,
  `BidTicket10` float DEFAULT NULL,
  `BidTicket11` float DEFAULT NULL,
  `BidTicket12` float DEFAULT NULL,
  `BidTicket13` float DEFAULT NULL,
  `BidTicket14` float DEFAULT NULL,
  `BidTicket15` float DEFAULT NULL,
  `BidTicket16` float DEFAULT NULL,
  `BidTicket17` float DEFAULT NULL,
  `BidTicket18` float DEFAULT NULL,
  `BidTicket19` float DEFAULT NULL,
  `Ask0` float DEFAULT NULL,
  `Ask1` float DEFAULT NULL,
  `Ask2` float DEFAULT NULL,
  `Ask3` float DEFAULT NULL,
  `Ask4` float DEFAULT NULL,
  `Ask5` float DEFAULT NULL,
  `Ask6` float DEFAULT NULL,
  `Ask7` float DEFAULT NULL,
  `Ask8` float DEFAULT NULL,
  `Ask9` float DEFAULT NULL,
  `Ask10` float DEFAULT NULL,
  `Ask11` float DEFAULT NULL,
  `Ask12` float DEFAULT NULL,
  `Ask13` float DEFAULT NULL,
  `Ask14` float DEFAULT NULL,
  `Ask15` float DEFAULT NULL,
  `Ask16` float DEFAULT NULL,
  `Ask17` float DEFAULT NULL,
  `Ask18` float DEFAULT NULL,
  `Ask19` float DEFAULT NULL,
  `AskQty0` float DEFAULT NULL,
  `AskQty1` float DEFAULT NULL,
  `AskQty2` float DEFAULT NULL,
  `AskQty3` float DEFAULT NULL,
  `AskQty4` float DEFAULT NULL,
  `AskQty5` float DEFAULT NULL,
  `AskQty6` float DEFAULT NULL,
  `AskQty7` float DEFAULT NULL,
  `AskQty8` float DEFAULT NULL,
  `AskQty9` float DEFAULT NULL,
  `AskQty10` float DEFAULT NULL,
  `AskQty11` float DEFAULT NULL,
  `AskQty12` float DEFAULT NULL,
  `AskQty13` float DEFAULT NULL,
  `AskQty14` float DEFAULT NULL,
  `AskQty15` float DEFAULT NULL,
  `AskQty16` float DEFAULT NULL,
  `AskQty17` float DEFAULT NULL,
  `AskQty18` float DEFAULT NULL,
  `AskQty19` float DEFAULT NULL,
  `AskTicket0` float DEFAULT NULL,
  `AskTicket1` float DEFAULT NULL,
  `AskTicket2` float DEFAULT NULL,
  `AskTicket3` float DEFAULT NULL,
  `AskTicket4` float DEFAULT NULL,
  `AskTicket5` float DEFAULT NULL,
  `AskTicket6` float DEFAULT NULL,
  `AskTicket7` float DEFAULT NULL,
  `AskTicket8` float DEFAULT NULL,
  `AskTicket9` float DEFAULT NULL,
  `AskTicket10` float DEFAULT NULL,
  `AskTicket11` float DEFAULT NULL,
  `AskTicket12` float DEFAULT NULL,
  `AskTicket13` float DEFAULT NULL,
  `AskTicket14` float DEFAULT NULL,
  `AskTicket15` float DEFAULT NULL,
  `AskTicket16` float DEFAULT NULL,
  `AskTicket17` float DEFAULT NULL,
  `AskTicket18` float DEFAULT NULL,
  `AskTicket19` float DEFAULT NULL,
  `Last0` float DEFAULT NULL,
  `Last1` float DEFAULT NULL,
  `Last2` float DEFAULT NULL,
  `Last3` float DEFAULT NULL,
  `Last4` float DEFAULT NULL,
  `Last5` float DEFAULT NULL,
  `Last6` float DEFAULT NULL,
  `Last7` float DEFAULT NULL,
  `Last8` float DEFAULT NULL,
  `Last9` float DEFAULT NULL,
  `Last10` float DEFAULT NULL,
  `Last11` float DEFAULT NULL,
  `Last12` float DEFAULT NULL,
  `Last13` float DEFAULT NULL,
  `Last14` float DEFAULT NULL,
  `Last15` float DEFAULT NULL,
  `Last16` float DEFAULT NULL,
  `Last17` float DEFAULT NULL,
  `Last18` float DEFAULT NULL,
  `Last19` float DEFAULT NULL,
  `LastQty0` float DEFAULT NULL,
  `LastQty1` float DEFAULT NULL,
  `LastQty2` float DEFAULT NULL,
  `LastQty3` float DEFAULT NULL,
  `LastQty4` float DEFAULT NULL,
  `LastQty5` float DEFAULT NULL,
  `LastQty6` float DEFAULT NULL,
  `LastQty7` float DEFAULT NULL,
  `LastQty8` float DEFAULT NULL,
  `LastQty9` float DEFAULT NULL,
  `LastQty10` float DEFAULT NULL,
  `LastQty11` float DEFAULT NULL,
  `LastQty12` float DEFAULT NULL,
  `LastQty13` float DEFAULT NULL,
  `LastQty14` float DEFAULT NULL,
  `LastQty15` float DEFAULT NULL,
  `LastQty16` float DEFAULT NULL,
  `LastQty17` float DEFAULT NULL,
  `LastQty18` float DEFAULT NULL,
  `LastQty19` float DEFAULT NULL,
  `LastTime0` int(11) DEFAULT NULL,
  `LastTime1` int(11) DEFAULT NULL,
  `LastTime2` int(11) DEFAULT NULL,
  `LastTime3` int(11) DEFAULT NULL,
  `LastTime4` int(11) DEFAULT NULL,
  `LastTime5` int(11) DEFAULT NULL,
  `LastTime6` int(11) DEFAULT NULL,
  `LastTime7` int(11) DEFAULT NULL,
  `LastTime8` int(11) DEFAULT NULL,
  `LastTime9` int(11) DEFAULT NULL,
  `LastTime10` int(11) DEFAULT NULL,
  `LastTime11` int(11) DEFAULT NULL,
  `LastTime12` int(11) DEFAULT NULL,
  `LastTime13` int(11) DEFAULT NULL,
  `LastTime14` int(11) DEFAULT NULL,
  `LastTime15` int(11) DEFAULT NULL,
  `LastTime16` int(11) DEFAULT NULL,
  `LastTime17` int(11) DEFAULT NULL,
  `LastTime18` int(11) DEFAULT NULL,
  `LastTime19` int(11) DEFAULT NULL,
  `Equil` double DEFAULT NULL,
  `Open` double DEFAULT NULL,
  `High` double DEFAULT NULL,
  `Low` double DEFAULT NULL,
  `Close` double DEFAULT NULL,
  `CloseDate` int(11) DEFAULT NULL,
  `TurnoverVol` double DEFAULT NULL,
  `TurnoverAmt` double DEFAULT NULL,
  `OpenInt` int(11) DEFAULT NULL,
  `ProdCode` varchar(100) DEFAULT NULL,
  `ProdName` varchar(100) DEFAULT NULL,
  `DecInPrice` varchar(1) DEFAULT NULL,
  `ExStateNo` int(11) DEFAULT NULL,
  `TradeStateNo` int(11) DEFAULT NULL,
  `Suspend` tinyint(1) DEFAULT NULL,
  `ExpiryYMD` int(11) DEFAULT NULL,
  `ContractYMD` int(11) DEFAULT NULL,
  `Timestamp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `subscribePrice`
--

CREATE TABLE IF NOT EXISTS `subscribePrice` (
  `id` int(11) NOT NULL,
  `username` varchar(60) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `ip` varchar(60) DEFAULT NULL,
  `license` varchar(60) DEFAULT NULL,
  `appId` varchar(60) DEFAULT NULL,
  `prodCode` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `subscribeTicker`
--

CREATE TABLE IF NOT EXISTS `subscribeTicker` (
  `id` int(11) NOT NULL,
  `username` varchar(60) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `ip` varchar(60) DEFAULT NULL,
  `license` varchar(60) DEFAULT NULL,
  `appId` varchar(60) DEFAULT NULL,
  `prodCode` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `tick`
--

CREATE TABLE IF NOT EXISTS `tick` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `username` varchar(60) DEFAULT NULL,
  `ip` varchar(60) DEFAULT NULL,
  `license` varchar(60) DEFAULT NULL,
  `appId` varchar(60) DEFAULT NULL,
  `prodCode` varchar(20) NOT NULL,
  `price` float NOT NULL,
  `qty` int(11) NOT NULL,
  `dealSrc` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `price`
--
ALTER TABLE `price`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `subscribePrice`
--
ALTER TABLE `subscribePrice`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `subscribeTicker`
--
ALTER TABLE `subscribeTicker`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tick`
--
ALTER TABLE `tick`
  ADD PRIMARY KEY (`id`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `price`
--
ALTER TABLE `price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用資料表 AUTO_INCREMENT `subscribePrice`
--
ALTER TABLE `subscribePrice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用資料表 AUTO_INCREMENT `subscribeTicker`
--
ALTER TABLE `subscribeTicker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用資料表 AUTO_INCREMENT `tick`
--
ALTER TABLE `tick`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
