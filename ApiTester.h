//---------------------------------------------------------------------------

#ifndef ApiTesterH
#define ApiTesterH
//---------------------------------------------------------------------------
#include<string.h>
#include "ApiProxyWrapper.h"
#include "ApiProxyWrapperReply.h"

extern string restUrl;

void convertTime(char buff[20], int32_t timeStamp);

class ApiTester : public ApiProxyWrapperReply {
public:
	vector<SPApiOrder *> orders;
	char host[256], user_id[256], password[256], acc_no[16], license[256], app_id[16];
	bool login_status = false;
	int port;
	int ACount = 1;

	int logined = -1;
	int accountLogined = -1;

	ApiTester(void);
	~ApiTester(void);

	void Init();
	void Unload();

	void GetAccNo(char *acc);
	void GetUserId(char *user);
	void HelpInfo();
	int Login();
	int LoginSync();
	void waitUntilLogin();

	void AddOrder();
	int AddOrderSync(SPApiOrder ord);
	void SendMMOrder();
	void DeleteOrder();
	int DeleteOrderSync(char userId[16], char accNo[16], char *prodCode, int orderNo);
	void DeleteAllOrders();
	int DeleteAllOrders(char userId[16], char accNo[16]);
	void ChangeOrder();
	void ChangeOrderBy();
	void GetActiveOrders();
	void SetOrderInactive();
	void GetOrderCount();
	int GetOrderCountSync(char userId[16], char accNo[16]);
	int GetOrdersByArray(char* userId, char* acc_no, SPApiOrder* apiOrderList);

	void PosCount();
	void GetPosByProduct();
	void TradeCount();
	void GetAllTrade();
	vector<SPApiTrade> GetAllTradesSync(char* userId, char* acc_no);

	void SubscribeUninitPrice(int idx);
	void SubscribeUninitPriceSync(int idx, char userId[16], char prodcode[16]);
	void GetPriceByCode();
	void SubscribeTicker();
	void SubscribeTickerSync(char userId[16], char prodcode[16]);
	void UninitTicker();
	void SubscribeQuoteRequest();
	void SubscribeAllQuoteRequest();

	void GetAccInfo();
	void GetAccBalCount();
	int GetAccBalCountSync(char userId[256]);
	void GetAllAccBal();
	void GetAllAccBalSync(char accNo[16], vector<SPApiAccBal> *apiAccBalList);
	void GetAccBalByCurrency();
	void GetConnectionStatus();
	void GetCcyRateByCcy();

	void ChangePassword();
	void SetApiLogPath();
	void AccountLogin();
	int AccountLoginSync(char accNo[16]);
	void AccountLoginSync();
	void AccountLogout();

	void LoadProductInfoListByCode();
	void GetProductCount();
	void GetProduct();
	void GetProductByCode();

	void LoadInstrumentList();
	void GetInstrumentCount();
	void GetInstrument();
	void GetInstrumentByCode();


	virtual void OnTest();
	virtual void OnLoginReply(long ret_code, char *ret_msg);
	virtual void OnConnectedReply(long host_type, long con_status);
	virtual void OnApiOrderRequestFailed(tinyint action, const SPApiOrder *order, long err_code, char *err_msg);
	virtual void OnApiOrderReport(long rec_no, const SPApiOrder *order);
	virtual void OnApiOrderBeforeSendReport(const SPApiOrder *order);
	virtual void OnAccountLoginReply(char *accNo, long ret_code, char* ret_msg);
	virtual void OnAccountLogoutReply(long ret_code, char* ret_msg);
	virtual void OnAccountInfoPush(const SPApiAccInfo *acc_info);
	virtual void OnAccountPositionPush(const SPApiPos *pos);
	virtual void OnUpdatedAccountPositionPush(const SPApiPos *pos);
	virtual void OnUpdatedAccountBalancePush(const SPApiAccBal *acc_bal);
	virtual void OnApiTradeReport(long rec_no, const SPApiTrade *trade);
	virtual void OnApiPriceUpdate(const SPApiPrice *price);
	virtual void OnApiTickerUpdate(const SPApiTicker *ticker);
	virtual void OnPswChangeReply(long ret_code, char *ret_msg);
	virtual void OnProductListByCodeReply(char *inst_code, bool is_ready, char *ret_msg);
	virtual void OnInstrumentListReply(bool is_ready, char *ret_msg);
	virtual void OnBusinessDateReply(long business_date);
	virtual void OnApiMMOrderBeforeSendReport(SPApiMMOrder *mm_order);
	virtual void OnApiMMOrderRequestFailed(SPApiMMOrder *mm_order, long err_code, char *err_msg);
	virtual void OnApiQuoteRequestReceived(char *product_code, char buy_sell, long qty);

	typedef void (*MyTickerHock)(const ApiTester *apiTester, const SPApiTicker *ticker);
	MyTickerHock tickerHock;
	typedef void (*MyPriceHock)(const ApiTester *apiTester, const SPApiPrice *price);
	MyPriceHock priceHock;
private:
	ApiProxyWrapper apiProxyWrapper;
};
#endif
