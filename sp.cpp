/* 
 * File: sp.c
 * Author: Peter <mcheung63@hotmail.com>
 * License: LGPL
 * Created on July 30, 2016, 1:25 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <curses.h>
#include <map>
#include <list>
#include <unordered_map>
#include <httpserver.hpp>
#include <boost/algorithm/string.hpp>

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>
#include "helper.h"

using namespace std;
using namespace httpserver;
using namespace sql;
using namespace boost;

#include "ApiTester.h"
#include "ApiAccount.h"
#include "Http_index.cpp"
#include "Http_subscribePrice.cpp"
#include "Http_subscribeTicker.cpp"
#include "Http_getAccountBalances.cpp"
#include "Http_getOrders.cpp"
#include "Http_getOrders2.cpp"
#include "Http_getTrades.cpp"
#include "Http_addOrder.cpp"
#include "Http_cancelAllOrders.cpp"
#include "Http_cancelOrder.cpp"

mutex loginMutex;
map<ApiAccount, ApiTester *> accounts;
string restUrl = "http://127.0.0.1:8080";

void myApiTickerUpdate(const ApiTester *apiTester, const SPApiTicker *ticker) {
	char buff[20];
	convertTime(buff, ticker->TickerTime);
	cout << "\tmyApiTickerUpdate : " + string(apiTester->user_id) + "\t" + string(ticker->ProdCode) << '\t' << ticker->Price << '\t' << ticker->Qty << "\t" << buff << endl;

	Driver *driver;
	Connection *con;
	PreparedStatement *prep_stmt;
	try {
		driver = get_driver_instance();
		con = driver->connect("tcp://127.0.0.1", "sp", "sp");
		con->setSchema("sp");
		prep_stmt = con->prepareStatement("insert into tick values (0, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

		prep_stmt->setString(1, buff);
		prep_stmt->setString(2, apiTester->user_id);
		string s = apiTester->host + string(":") + to_string(apiTester->port);
		prep_stmt->setString(3, s.c_str());
		prep_stmt->setString(4, apiTester->license);
		prep_stmt->setString(5, apiTester->app_id);
		prep_stmt->setString(6, ticker->ProdCode);
		prep_stmt->setDouble(7, ticker->Price);
		prep_stmt->setInt(8, ticker->Qty);
		prep_stmt->setInt(9, ticker->DealSrc);
		prep_stmt->execute();
	} catch (SQLException &e) {
		cout << "# ERR: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# ERR: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	}

	delete prep_stmt;
	delete con;
}

void myApiPriceUpdate(const ApiTester *apiTester, const SPApiPrice *price) {
	char buff[20];
	convertTime(buff, price->Timestamp);
	cout << "\tmyApiPriceUpdate : " + string(apiTester->user_id) + "\t" + string(price->ProdCode) << '\t' << price->BidQty[0] << '\t' << price->Bid[0] << '\t' << price->Ask[0] << '\t' << price->AskQty[0] << '\t' << buff << endl;

	Driver *driver;
	Connection *con;
	PreparedStatement *prep_stmt;
	try {
		driver = get_driver_instance();
		con = driver->connect("tcp://127.0.0.1", "sp", "sp");
		con->setSchema("sp");
		Statement *stmt = con->createStatement();
		ResultSet *res = stmt->executeQuery("SELECT count(*) FROM information_schema.columns WHERE table_name = 'price'");
		res->next();
		int count = res->getInt("count(*)");
		string sql = "insert into price values (0 ";
		for (int z = 0; z < count - 1; z++) {
			sql += string(", ?");
		}
		sql += ")";
		delete res;
		delete stmt;
		prep_stmt = con->prepareStatement(sql);

		int x = 1;
		prep_stmt->setString(x++, buff);
		prep_stmt->setString(x++, apiTester->user_id);
		string s = apiTester->host + string(":") + to_string(apiTester->port);
		prep_stmt->setString(x++, s.c_str());
		prep_stmt->setString(x++, apiTester->license);
		prep_stmt->setString(x++, apiTester->app_id);
		for (int i = 0; i < 20; i++, x++) {
			prep_stmt->setDouble(x, price->Bid[i]);
		}
		for (int i = 0; i < 20; i++, x++) {
			prep_stmt->setInt(x, price->BidQty[i]);
		}
		for (int i = 0; i < 20; i++, x++) {
			prep_stmt->setInt(x, price->BidTicket[i]);
		}
		for (int i = 0; i < 20; i++, x++) {
			prep_stmt->setDouble(x, price->Ask[i]);
		}
		for (int i = 0; i < 20; i++, x++) {
			prep_stmt->setInt(x, price->AskQty[i]);
		}
		for (int i = 0; i < 20; i++, x++) {
			prep_stmt->setInt(x, price->AskTicket[i]);
		}
		for (int i = 0; i < 20; i++, x++) {
			prep_stmt->setDouble(x, price->Last[i]);
		}
		for (int i = 0; i < 20; i++, x++) {
			prep_stmt->setInt(x, price->LastQty[i]);
		}
		for (int i = 0; i < 20; i++, x++) {
			prep_stmt->setInt(x, price->LastTime[i]);
		}
		prep_stmt->setDouble(x++, price->Equil);
		prep_stmt->setDouble(x++, price->Open);
		prep_stmt->setDouble(x++, price->High);
		prep_stmt->setDouble(x++, price->Low);
		prep_stmt->setDouble(x++, price->Close);
		prep_stmt->setInt(x++, price->CloseDate);
		prep_stmt->setDouble(x++, price->TurnoverVol);
		prep_stmt->setDouble(x++, price->TurnoverAmt);
		prep_stmt->setInt(x++, price->OpenInt);
		prep_stmt->setString(x++, price->ProdCode);
		prep_stmt->setString(x++, price->ProdName);
		prep_stmt->setString(x++, to_string(price->DecInPrice));
		prep_stmt->setInt(x++, price->ExStateNo);
		prep_stmt->setInt(x++, price->TradeStateNo);
		prep_stmt->setBoolean(x++, price->Suspend);
		prep_stmt->setInt(x++, price->ExpiryYMD);
		prep_stmt->setInt(x++, price->ContractYMD);
		prep_stmt->setInt(x++, price->Timestamp);
		prep_stmt->execute();
	} catch (SQLException &e) {
		cout << "# ERR: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# ERR: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	}

	delete prep_stmt;
	delete con;
}

void startRestServer() {
	cout << "started restful api server" << endl;
	webserver ws = create_webserver(8686).start_method(http::http_utils::INTERNAL_SELECT).max_threads(5);
	cout << "started restful api server 2" << endl;
	//ws.register_resource("/", new Http_index(), true);
	ws.register_resource("/", new Http_index(), true);
	cout << "started restful api server 3" << endl;
	ws.register_resource("/subscribeTicker", new Http_subscribeTicker(), true);
	ws.register_resource("/subscribePrice", new Http_subscribePrice(), true);
	ws.register_resource("/getAccountBalances", new Http_getAccountBalances(), true);
	ws.register_resource("/getOrders", new Http_getOrders(), true);
	ws.register_resource("/getOrders2", new Http_getOrders2(), true);
		cout << "started restful api server 4" << endl;
	ws.register_resource("/getTrades", new Http_getTrades(), true);
	ws.register_resource("/addOrder", new Http_addOrder(), true);
	ws.register_resource("/cancelAllOrders", new Http_cancelAllOrders(), true);
	ws.register_resource("/cancelOrder", new Http_cancelOrder(), true);
	cout << "started restful api server 5" << endl;
	ws.start(true);
	cout << "started restful api server end" << endl;
}

void initMysql() {
	log("init mysql");
	Driver *driver;
	Connection *con;
	Statement *stmt;
	try {
		driver = get_driver_instance();
		con = driver->connect("tcp://127.0.0.1", "sp", "sp");
		con->setSchema("sp");
		stmt = con->createStatement();
		stmt->execute("create table if not exists `tick` (`id` int(11) primary key auto_increment,`date` datetime NOT NULL,username varchar(60) null, ip varchar(60) null, license varchar(60) null, appId varchar(60) null, `prodCode` varchar(20) NOT NULL,`price` float NOT NULL,`qty` int(11) NOT NULL, `dealSrc` int(11) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

		stmt->execute("create table if not exists `price` ( \
			`id` int(11) primary key auto_increment, \
			`date` datetime DEFAULT NULL, \
			`username` varchar(60) DEFAULT NULL, \
			`ip` varchar(60) NOT NULL, \
			`license` varchar(60) DEFAULT NULL, \
			`appId` varchar(60) DEFAULT NULL, \
			`Bid0` float DEFAULT NULL, \
			`Bid1` float DEFAULT NULL, \
			`Bid2` float DEFAULT NULL, \
			`Bid3` float DEFAULT NULL, \
			`Bid4` float DEFAULT NULL, \
			`Bid5` float DEFAULT NULL, \
			`Bid6` float DEFAULT NULL, \
			`Bid7` float DEFAULT NULL, \
			`Bid8` float DEFAULT NULL, \
			`Bid9` float DEFAULT NULL, \
			`Bid10` float DEFAULT NULL, \
			`Bid11` float DEFAULT NULL, \
			`Bid12` float DEFAULT NULL, \
			`Bid13` float DEFAULT NULL, \
			`Bid14` float DEFAULT NULL, \
			`Bid15` float DEFAULT NULL, \
			`Bid16` float DEFAULT NULL, \
			`Bid17` float DEFAULT NULL, \
			`Bid18` float DEFAULT NULL, \
			`Bid19` float DEFAULT NULL, \
			`BidQty0` float DEFAULT NULL, \
			`BidQty1` float DEFAULT NULL, \
			`BidQty2` float DEFAULT NULL, \
			`BidQty3` float DEFAULT NULL, \
			`BidQty4` float DEFAULT NULL, \
			`BidQty5` float DEFAULT NULL, \
			`BidQty6` float DEFAULT NULL, \
			`BidQty7` float DEFAULT NULL, \
			`BidQty8` float DEFAULT NULL, \
			`BidQty9` float DEFAULT NULL, \
			`BidQty10` float DEFAULT NULL, \
			`BidQty11` float DEFAULT NULL, \
			`BidQty12` float DEFAULT NULL, \
			`BidQty13` float DEFAULT NULL, \
			`BidQty14` float DEFAULT NULL, \
			`BidQty15` float DEFAULT NULL, \
			`BidQty16` float DEFAULT NULL, \
			`BidQty17` float DEFAULT NULL, \
			`BidQty18` float DEFAULT NULL, \
			`BidQty19` float DEFAULT NULL, \
			`BidTicket0` float DEFAULT NULL, \
			`BidTicket1` float DEFAULT NULL, \
			`BidTicket2` float DEFAULT NULL, \
			`BidTicket3` float DEFAULT NULL, \
			`BidTicket4` float DEFAULT NULL, \
			`BidTicket5` float DEFAULT NULL, \
			`BidTicket6` float DEFAULT NULL, \
			`BidTicket7` float DEFAULT NULL, \
			`BidTicket8` float DEFAULT NULL, \
			`BidTicket9` float DEFAULT NULL, \
			`BidTicket10` float DEFAULT NULL, \
			`BidTicket11` float DEFAULT NULL, \
			`BidTicket12` float DEFAULT NULL, \
			`BidTicket13` float DEFAULT NULL, \
			`BidTicket14` float DEFAULT NULL, \
			`BidTicket15` float DEFAULT NULL, \
			`BidTicket16` float DEFAULT NULL, \
			`BidTicket17` float DEFAULT NULL, \
			`BidTicket18` float DEFAULT NULL, \
			`BidTicket19` float DEFAULT NULL, \
			`Ask0` float DEFAULT NULL, \
			`Ask1` float DEFAULT NULL, \
			`Ask2` float DEFAULT NULL, \
			`Ask3` float DEFAULT NULL, \
			`Ask4` float DEFAULT NULL, \
			`Ask5` float DEFAULT NULL, \
			`Ask6` float DEFAULT NULL, \
			`Ask7` float DEFAULT NULL, \
			`Ask8` float DEFAULT NULL, \
			`Ask9` float DEFAULT NULL, \
			`Ask10` float DEFAULT NULL, \
			`Ask11` float DEFAULT NULL, \
			`Ask12` float DEFAULT NULL, \
			`Ask13` float DEFAULT NULL, \
			`Ask14` float DEFAULT NULL, \
			`Ask15` float DEFAULT NULL, \
			`Ask16` float DEFAULT NULL, \
			`Ask17` float DEFAULT NULL, \
			`Ask18` float DEFAULT NULL, \
			`Ask19` float DEFAULT NULL, \
			`AskQty0` float DEFAULT NULL, \
			`AskQty1` float DEFAULT NULL, \
			`AskQty2` float DEFAULT NULL, \
			`AskQty3` float DEFAULT NULL, \
			`AskQty4` float DEFAULT NULL, \
			`AskQty5` float DEFAULT NULL, \
			`AskQty6` float DEFAULT NULL, \
			`AskQty7` float DEFAULT NULL, \
			`AskQty8` float DEFAULT NULL, \
			`AskQty9` float DEFAULT NULL, \
			`AskQty10` float DEFAULT NULL, \
			`AskQty11` float DEFAULT NULL, \
			`AskQty12` float DEFAULT NULL, \
			`AskQty13` float DEFAULT NULL, \
			`AskQty14` float DEFAULT NULL, \
			`AskQty15` float DEFAULT NULL, \
			`AskQty16` float DEFAULT NULL, \
			`AskQty17` float DEFAULT NULL, \
			`AskQty18` float DEFAULT NULL, \
			`AskQty19` float DEFAULT NULL, \
			`AskTicket0` float DEFAULT NULL, \
			`AskTicket1` float DEFAULT NULL, \
			`AskTicket2` float DEFAULT NULL, \
			`AskTicket3` float DEFAULT NULL, \
			`AskTicket4` float DEFAULT NULL, \
			`AskTicket5` float DEFAULT NULL, \
			`AskTicket6` float DEFAULT NULL, \
			`AskTicket7` float DEFAULT NULL, \
			`AskTicket8` float DEFAULT NULL, \
			`AskTicket9` float DEFAULT NULL, \
			`AskTicket10` float DEFAULT NULL, \
			`AskTicket11` float DEFAULT NULL, \
			`AskTicket12` float DEFAULT NULL, \
			`AskTicket13` float DEFAULT NULL, \
			`AskTicket14` float DEFAULT NULL, \
			`AskTicket15` float DEFAULT NULL, \
			`AskTicket16` float DEFAULT NULL, \
			`AskTicket17` float DEFAULT NULL, \
			`AskTicket18` float DEFAULT NULL, \
			`AskTicket19` float DEFAULT NULL, \
			`Last0` float DEFAULT NULL, \
			`Last1` float DEFAULT NULL, \
			`Last2` float DEFAULT NULL, \
			`Last3` float DEFAULT NULL, \
			`Last4` float DEFAULT NULL, \
			`Last5` float DEFAULT NULL, \
			`Last6` float DEFAULT NULL, \
			`Last7` float DEFAULT NULL, \
			`Last8` float DEFAULT NULL, \
			`Last9` float DEFAULT NULL, \
			`Last10` float DEFAULT NULL, \
			`Last11` float DEFAULT NULL, \
			`Last12` float DEFAULT NULL, \
			`Last13` float DEFAULT NULL, \
			`Last14` float DEFAULT NULL, \
			`Last15` float DEFAULT NULL, \
			`Last16` float DEFAULT NULL, \
			`Last17` float DEFAULT NULL, \
			`Last18` float DEFAULT NULL, \
			`Last19` float DEFAULT NULL, \
			`LastQty0` float DEFAULT NULL, \
			`LastQty1` float DEFAULT NULL, \
			`LastQty2` float DEFAULT NULL, \
			`LastQty3` float DEFAULT NULL, \
			`LastQty4` float DEFAULT NULL, \
			`LastQty5` float DEFAULT NULL, \
			`LastQty6` float DEFAULT NULL, \
			`LastQty7` float DEFAULT NULL, \
			`LastQty8` float DEFAULT NULL, \
			`LastQty9` float DEFAULT NULL, \
			`LastQty10` float DEFAULT NULL, \
			`LastQty11` float DEFAULT NULL, \
			`LastQty12` float DEFAULT NULL, \
			`LastQty13` float DEFAULT NULL, \
			`LastQty14` float DEFAULT NULL, \
			`LastQty15` float DEFAULT NULL, \
			`LastQty16` float DEFAULT NULL, \
			`LastQty17` float DEFAULT NULL, \
			`LastQty18` float DEFAULT NULL, \
			`LastQty19` float DEFAULT NULL, \
			`LastTime0` int DEFAULT NULL, \
			`LastTime1` int DEFAULT NULL, \
			`LastTime2` int DEFAULT NULL, \
			`LastTime3` int DEFAULT NULL, \
			`LastTime4` int DEFAULT NULL, \
			`LastTime5` int DEFAULT NULL, \
			`LastTime6` int DEFAULT NULL, \
			`LastTime7` int DEFAULT NULL, \
			`LastTime8` int DEFAULT NULL, \
			`LastTime9` int DEFAULT NULL, \
			`LastTime10` int DEFAULT NULL, \
			`LastTime11` int DEFAULT NULL, \
			`LastTime12` int DEFAULT NULL, \
			`LastTime13` int DEFAULT NULL, \
			`LastTime14` int DEFAULT NULL, \
			`LastTime15` int DEFAULT NULL, \
			`LastTime16` int DEFAULT NULL, \
			`LastTime17` int DEFAULT NULL, \
			`LastTime18` int DEFAULT NULL, \
			`LastTime19` int DEFAULT NULL, \
			 Equil double DEFAULT NULL, \
			 Open double DEFAULT NULL, \
			 High double DEFAULT NULL, \
			 Low double DEFAULT NULL, \
			 Close double DEFAULT NULL, \
			 CloseDate int DEFAULT NULL, \
			 TurnoverVol double DEFAULT NULL, \
			 TurnoverAmt double DEFAULT NULL, \
			 OpenInt int DEFAULT NULL, \
			 ProdCode varchar(100) DEFAULT NULL, \
			 ProdName varchar(100) DEFAULT NULL, \
			 DecInPrice varchar(1) DEFAULT NULL, \
			 ExStateNo int DEFAULT NULL, \
			 TradeStateNo int DEFAULT NULL, \
			 Suspend boolean DEFAULT NULL, \
			 ExpiryYMD int DEFAULT NULL, \
			 ContractYMD int DEFAULT NULL, \
			 Timestamp int DEFAULT NULL \
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

		stmt->execute("create table if not exists `subscribeTicker` (`id` int(11) primary key auto_increment, username varchar(60) null, password varchar(60) null, ip varchar(60) null, license varchar(60) null, appId varchar(60) null, `prodCode` varchar(20) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
		stmt->execute("create table if not exists `subscribePrice` (`id` int(11) primary key auto_increment, username varchar(60) null, password varchar(60) null, ip varchar(60) null, license varchar(60) null, appId varchar(60) null, `prodCode` varchar(20) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
	} catch (SQLException &e) {
		cout << "# ERR: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# ERR: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
		exit(1);
	}
	delete stmt;
	delete con;

	log("init mysql end");
}

/*
void loadSubscribedTickAndPrice() {
	Driver *driver;
	Connection *con;
	PreparedStatement *prep_stmt;
	try {
		driver = get_driver_instance();
		con = driver->connect("tcp://127.0.0.1", "sp", "sp");
		con->setSchema("sp");
		prep_stmt = con->prepareStatement("select * from subscribeTicker");
		ResultSet *sqlRes = prep_stmt->executeQuery();
		while (sqlRes->next()) {
			//int count = sqlRes->getInt("count(*)");

			vector<string> results;
			string temp = sqlRes->getString("ip");
			split(results, temp, is_any_of(":"));

			string ip = results[0];
			string portstr = results[1];
			string username = sqlRes->getString("username");
			string password = sqlRes->getString("password");
			string license = sqlRes->getString("license");
			string appId = sqlRes->getString("appId");
			string prodCode = sqlRes->getString("prodCode");

			ApiAccount apiAccount(ip, username, password, license, appId);
			ApiTester *apiTester = accounts[apiAccount];
			http_response** res;
			if (apiTester == NULL) {
				LOGIN_MARCO
			}
			apiTester->waitUntilLogin();
			cout << "subscribe ticker apiTester=" << apiTester << ", username=" << username << " = " << prodCode << endl;
			apiTester->SubscribeTickerSync((char *) username.c_str(), (char *) prodCode.c_str());
			cout << "subscribe ticker apiTester=" << apiTester << ", username=" << username << " = " << prodCode << " end" << endl;
		}
		delete sqlRes;
		delete prep_stmt;

		prep_stmt = con->prepareStatement("select * from subscribePrice");
		sqlRes = prep_stmt->executeQuery();
		while (sqlRes->next()) {
			//int count = sqlRes->getInt("count(*)");

			vector<string> results;
			string temp = sqlRes->getString("ip");
			split(results, temp, is_any_of(":"));

			string ip = results[0];
			string portstr = results[1];
			string username = sqlRes->getString("username");
			string password = sqlRes->getString("password");
			string license = sqlRes->getString("license");
			string appId = sqlRes->getString("appId");
			string prodCode = sqlRes->getString("prodCode");

			ApiAccount apiAccount(ip, username, password, license, appId);
			ApiTester *apiTester = accounts[apiAccount];
			http_response** res;
			if (apiTester == NULL) {
				LOGIN_MARCO
			}
			apiTester->waitUntilLogin();
			cout << "subscribe price apiTester=" << apiTester << ", username=" << username << " = " << prodCode << endl;
			apiTester->SubscribeUninitPriceSync(1, (char *) username.c_str(), (char *) prodCode.c_str());
			cout << "subscribe price apiTester=" << apiTester << ", username=" << username << " = " << prodCode << " end" << endl;
		}
	} catch (SQLException &e) {
		cout << "# ERR: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# ERR: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	}

	delete con;
}
*/

int main(int argc, char** argv) {
	log("Quantr-sharppoint");
	//initscr();
	//	cbreak();
	//	noecho();
	//	int h, w;
	//	getmaxyx(stdscr, h, w);
	//	cout << "h=" << h << ", w=" << w << endl;


	initMysql();
	//loadSubscribedTickAndPrice();
	startRestServer();

	/*
	ApiTester apiTester;
	strcpy(apiTester.host, "202.76.62.239");
	strcpy(apiTester.user_id, "206970");
	strcpy(apiTester.password, "bonnie36DD");
	apiTester.port = 80;
	strcpy(apiTester.app_id, "CHUYSSAM");
	strcpy(apiTester.license, "D044717EA505D7D7");
	string userId = "206970";
	
//	strcpy(apiTester.host, "demo.istartrade.com");
//	strcpy(apiTester.user_id, "DEMO201606003");
//	strcpy(apiTester.password, "12345678");
//	apiTester.port = 80;
//	strcpy(apiTester.license, "DEMO2016");
//	strcpy(apiTester.app_id, "ISTARDEMO");
//	string userId = "DEMO201606003";


	int rc = apiTester.LoginSync();
	if (rc != 1) {
		cout << "login failed" << endl;
		return 1;
	}
	cout << "logined" << endl;

	rc = apiTester.AccountLoginSync((char *) userId.c_str());
	cout << "account login=" << rc << endl;
	int balCount = apiTester.GetAccBalCountSync((char *) userId.c_str());
	cout << "balCount=" << balCount << endl;


	vector<SPApiAccBal> apiAccBalList;
	apiTester.GetAllAccBalSync((char *) userId.c_str(), &apiAccBalList);
	cout << "apiAccBalList.size()=" << apiAccBalList.size() << endl;
	for (int i = 0; i < apiAccBalList.size(); i++) {
		SPApiAccBal& accBal = apiAccBalList[i];
		printf("\t-No: %d Ccy=%s , CashBf=%f\n", i + 1, accBal.Ccy, accBal.CashBf);
	}

	apiTester.SubscribeTickerSync((char *) userId.c_str(), "HSIQ6");
	apiTester.SubscribeTickerSync((char *) userId.c_str(), "HHIQ6");
	apiTester.SubscribeTickerSync((char *) userId.c_str(), "0005.HK");
	apiTester.SubscribeUninitPriceSync(1, (char *) userId.c_str(), "HSIQ6");
	apiTester.SubscribeUninitPriceSync(1, (char *) userId.c_str(), "HHIQ6");
	apiTester.SubscribeUninitPriceSync(1, (char *) userId.c_str(), "0005.HK");

	apiTester.Unload();

	while (1) {
		usleep(100000);
	}
	 */
	return (EXIT_SUCCESS);
}
