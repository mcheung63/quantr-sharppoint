class Http_addOrder : public http_resource {

	const http_response render(const http_request& req){
		string ip = req.get_arg("ip");
		string username = req.get_arg("username");
		string password = req.get_arg("password");
		string license = req.get_arg("license");
		string appId = req.get_arg("appId");
		string portstr = req.get_arg("port");
		string prodCode = req.get_arg("prodCode");
		string price = req.get_arg("price");
		string qty = req.get_arg("qty");
		string ref = req.get_arg("ref");
		string ref2 = req.get_arg("ref2");
		string isBuy = req.get_arg("isBuy");
		string str;

		bool error = FALSE;
		check(&str, "ip", ip, &error);
		check(&str, "username", username, &error);
		check(&str, "password", password, &error);
		check(&str, "license", license, &error);
		check(&str, "appId", appId, &error);
		check(&str, "port", portstr, &error);
		check(&str, "prodCode", prodCode, &error);
		check(&str, "price", price, &error);
		check(&str, "qty", qty, &error);
		check(&str, "ref", ref, &error);
		check(&str, "ref2", ref2, &error);
		check(&str, "isBuy", isBuy, &error);

		ApiAccount apiAccount(ip, username, password, license, appId);

		if (!error) {
			LOGIN

			/*
			typedef struct
			{
				double Price;              //价格
				double StopLevel;          //止损价格
				double UpLevel;            //上限水平
				double UpPrice;            //上限价格
				double DownLevel;          //下限水平
				double DownPrice;          //下限价格
				bigint ExtOrderNo;         //外部指示
				int32_t IntOrderNo;           //用户订单编号
				int32_t Qty;                  //剩余数量
				int32_t TradedQty;            //已成交数量
				int32_t TotalQty;             //全部数量
				int32_t ValidTime;            //有效时间
				int32_t SchedTime;            //预订发送时间
				int32_t TimeStamp;            //服务器接收订单时间
				uint32_t OrderOptions;
				STR16 AccNo;               //用户帐号
				STR16 ProdCode;            //合约代号
				STR16 Initiator;           //下单用户
				STR16 Ref;                 //参考
				STR16 Ref2;                //参考2
				STR16 GatewayCode;         //网关
				STR40 ClOrderId;           //用户自定义指令代号
				char BuySell;              //买卖方向
				char StopType;             //止损类型
				char OpenClose;            //开平仓
				tinyint CondType;          //订单条件类型
				tinyint OrderType;         //订单类型
				tinyint ValidType;         //订单有效类型
				tinyint Status;            //状态
				tinyint DecInPrice;        //合约小数位
				tinyint OrderAction;
				int32_t UpdateTime;
				int32_t UpdateSeqNo;
			} SPApiOrder;
			 */

			loginMutex.lock();
			SPApiOrder ord;
			strcpy(ord.Ref, ref.c_str());
			strcpy(ord.Ref2, "");
			strcpy(ord.Initiator, username.c_str());
			strcpy(ord.AccNo, username.c_str());
			strcpy(ord.GatewayCode, "");
			ord.ValidType = 0;
			ord.CondType = 0;
			ord.DecInPrice = 0;
			ord.OrderOptions = 0; //1 = T+1
			ord.OrderType = ORD_LIMIT;
			//ord.OpenClose = OC_DEFAULT;
			ord.Qty = atoi(qty.c_str());
			if (isBuy == "true") {
				ord.BuySell = 'B';
			} else if (isBuy == "false") {
				ord.BuySell = 'S';
			} else {
				str = "wrong isBuy = \n";
				//loginMutex.unlock();
				//break;
			}
			ord.TradedQty = 0;
			ord.ValidTime = 0;
			ord.Price = stod(price);
			//ord.StopType = STOP_LOSS;
			strcpy(ord.ProdCode, prodCode.c_str());
			int rc = apiTester->AddOrderSync(ord);
			cout << "AddOrder rc=" << rc << endl;
			str = "AddOrder rc=" + to_string(rc) + "\n";
			loginMutex.unlock();
		} else {
			return http_response_builder("error, wrong parameter", 500).string_response();
		}
		cout << "------------------------------------------------------------------" << endl;
		return http_response_builder(str, 200).string_response();
	}
};

