
#include "ApiTester.h"

class Http_getTrades : public http_resource {

	const http_response render(const http_request& req){
		string ip = req.get_arg("ip");
		string username = req.get_arg("username");
		string password = req.get_arg("password");
		string license = req.get_arg("license");
		string appId = req.get_arg("appId");
		string portstr = req.get_arg("port");
		string str;
		bool error = FALSE;
		check(&str, "ip", ip, &error);
		check(&str, "username", username, &error);
		check(&str, "password", password, &error);
		check(&str, "license", license, &error);
		check(&str, "appId", appId, &error);
		check(&str, "port", portstr, &error);

		ApiAccount apiAccount(ip, username, password, license, appId);

		if (!error) {
			LOGIN
			cout << "getTrades " << username << endl;

			vector<SPApiTrade> trades = apiTester->GetAllTradesSync((char *) username.c_str(), (char *) username.c_str());
			//					
			//			int r = apiTester->GetOrdersByArray((char *) username.c_str(), (char *) username.c_str(), apiOrderList);
			//			cout << "r=" << r << endl;
			//
			str = "";
			for (SPApiTrade trade : trades) {
				//				str += string(order.AccNo) + "\t" + string(outputOrderStatus(order.Status)) + "\t" + to_string(order.IntOrderNo) + "\t" + string(order.ProdCode);
				//				str += "\t" + string(1, order.BuySell) + "\t" + to_string(order.Price) + "\t" + to_string(order.Qty) + "\t" + to_string(order.TradedQty) + "\t" + to_string(order.TotalQty);
				//				str += "\t" + string(order.ClOrderId) + "\n";
				printf(" Order#%d, TradeNo:%lld,ProdCode=%s , B/S=%c , Qty=%d, Price=%f\n", trade.IntOrderNo, trade.TradeNo, trade.ProdCode, trade.BuySell, trade.Qty, trade.Price);
				str += "Order#=" + to_string(trade.IntOrderNo) + ", TradeNo=" + to_string(trade.TradeNo) + ",ProdCode=" + string(trade.ProdCode) + ", B/S=" + (char)(trade.BuySell) + ", Qty=" + to_string(trade.Qty) + ", Price=" + to_string(trade.Price) + "\n";
			}
			//delete &trades;

		} else {
			return http_response_builder("error, wrong parameter", 500).string_response();
		}

		cout << "------------------------------------------------------------------" << endl;
		return http_response_builder(str, 200).string_response();
	}
};

