
#include "ApiTester.h"

class Http_cancelAllOrders : public http_resource {

	const http_response render(const http_request& req){
		string ip = req.get_arg("ip");
		string username = req.get_arg("username");
		string password = req.get_arg("password");
		string license = req.get_arg("license");
		string appId = req.get_arg("appId");
		string portstr = req.get_arg("port");
		string str;
		bool error = FALSE;
		check(&str, "ip", ip, &error);
		check(&str, "username", username, &error);
		check(&str, "password", password, &error);
		check(&str, "license", license, &error);
		check(&str, "appId", appId, &error);
		check(&str, "port", portstr, &error);

		ApiAccount apiAccount(ip, username, password, license, appId);

		if (!error) {
			LOGIN
			cout << "cancel all orders " << username << endl;

			int count = apiTester->GetOrderCountSync((char *) username.c_str(), (char *) username.c_str());
			str += to_string(count) + "\n";
			//			for (int x = 0; x < count; x++) {
			//				//apiTester->get
			//			}
			if (count <= 0) {
				return http_response_builder(str, 200).string_response();
			}
			
			apiTester->DeleteAllOrders((char *) username.c_str(), (char *) username.c_str());

//			SPApiOrder apiOrderList[count];
//			int r = apiTester->GetOrdersByArray((char *) username.c_str(), (char *) username.c_str(), apiOrderList);
//			for (int x = 0; x < count; x++) {
//				SPApiOrder order = apiOrderList[x];
//				str += to_string(x) + "\t" + string(order.AccNo) + "\t" + string(outputOrderStatus(order.Status)) + "\t" + to_string(order.IntOrderNo) + "\t" + string(order.ProdCode);
//				str += "\t" + string(1, order.BuySell) + "\t" + to_string(order.Price) + "\t" + to_string(order.Qty) + "\t" + to_string(order.TradedQty) + "\t" + to_string(order.TotalQty);
//				str += "\t" + to_string(order.IntOrderNo) + "\n";
//				//				cout << "strlen(order.ClOrderId)=" << strlen(order.ClOrderId) << endl;
//				//				for (int y = 0; y < strlen(order.ClOrderId); y++) {
//				//					printf("%x ", order.ClOrderId[y]);
//				//				}
//				//				cout << endl;
//				//				cout << "ClOrderId=" << string(order.ClOrderId) << endl;
//			}

		} else {
			return http_response_builder("error, wrong parameter", 500).string_response();
		}

		cout << "------------------------------------------------------------------" << endl;
		return http_response_builder(str, 200).string_response();
	}
};

