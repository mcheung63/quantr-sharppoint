#include <iostream>
#include "ApiTester.h"

using namespace std;

int main(int argc, const char* argv[]);

int main(int argc, const char* argv[]) {
	int ch;
	ApiTester apiTester;
	apiTester.HelpInfo();
	apiTester.Login();
	while (1) {
		ch = getchar();
		switch (ch) {
			case 49: apiTester.Login();
				break; //Num 1							
			case 57: apiTester.HelpInfo();
				break; //Num 9

			case 113: apiTester.AddOrder();
				break; // q 
			case 81: apiTester.SendMMOrder();
				break; // Q
			case 119: apiTester.DeleteOrder();
				break; // w                           
			case 101: apiTester.ChangeOrder();
				break; // e   
			case 114: apiTester.GetActiveOrders();
				break; // r
			case 116: apiTester.SetOrderInactive();
				break; // t
			case 121: apiTester.GetOrderCount();
				break; // y

			case 117: apiTester.PosCount();
				break; // u
			case 105: apiTester.GetPosByProduct();
				break; // i
			case 111: apiTester.TradeCount();
				break; // o
			case 112: apiTester.GetAllTrade();
				break; // p

			case 97: apiTester.SubscribeUninitPrice(1);
				break; // a
			case 115: apiTester.SubscribeUninitPrice(0);
				break; // s
			case 100: apiTester.GetPriceByCode();
				break; // d
			case 102: apiTester.SubscribeQuoteRequest();
				break; // f
			case 103: apiTester.SubscribeAllQuoteRequest();
				break; // g
			case 104: apiTester.SubscribeTicker();
				break; // h
			case 106: apiTester.UninitTicker();
				break; // j

			case 107: apiTester.LoadProductInfoListByCode();
				break; // k
			case 108: apiTester.GetProduct();
				break; // l
			case 59: apiTester.GetProductByCode();
				break; // ;
			case 39: apiTester.GetProductCount();
				break; // '
			case 92: apiTester.LoadInstrumentList();
				break; // ' \'
			case 47: apiTester.GetInstrument();
				break; // /
			case 46: apiTester.GetInstrumentByCode();
				break; // .
			case 44: apiTester.GetInstrumentCount();
				break; // ,

			case 122: apiTester.GetAccInfo();
				break; // z
			case 120: apiTester.GetAccBalCount();
				break; // x
			case 99: apiTester.GetAllAccBal();
				break; // c
			case 118: apiTester.GetAccBalByCurrency();
				break; // v
			case 98: apiTester.GetConnectionStatus();
				break; // b
			case 110: apiTester.GetCcyRateByCcy();
				break; // n

			case 45: apiTester.ChangePassword();
				break; // -
			case 61: apiTester.SetApiLogPath();
				break; // =
			case 91: apiTester.AccountLogin();
				break; // [
			case 93: apiTester.AccountLogout();
				break; // ]
		}
		usleep(1000);
	}

	apiTester.Unload();
	return 0;
}
