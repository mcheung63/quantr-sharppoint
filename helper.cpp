#include "helper.h"

void check(string *str, string name, string field, bool *error) {
	if (field == "") {
		*str += name + " is empty\n";
		*error = true;
	}
}

char *outputOrderStatus(char status) {
	switch (status) {
		case ORDSTAT_SENDING: return"sending";
		case ORDSTAT_WORKING: return"working";
		case ORDSTAT_INACTIVE: return"inactive";
		case ORDSTAT_PENDING: return"pending";
		case ORDSTAT_ADDING: return"adding";
		case ORDSTAT_CHANGING: return"changing";
		case ORDSTAT_DELETING: return"deleting";
		case ORDSTAT_INACTING: return"inacting";
		case ORDSTAT_PARTTRD_WRK: return"parttrd_wrk";
		case ORDSTAT_TRADED: return"traded";
		case ORDSTAT_DELETED: return"deleted";
		case ORDSTAT_APPROVEWAIT: return"approve wait";
		case ORDSTAT_TRADEDREP: return"traded & reported";
		case ORDSTAT_DELETEDREP: return"deleted & reported";
		case ORDSTAT_RESYNC_ABN: return"resync abnormal";
		case ORDSTAT_PARTTRD_DEL: return"partial traded & deleted";
		case ORDSTAT_PARTTRD_REP: return"partial traded & reported (deleted)";
	}
	return "unknown";
}

//void log(char *message) {
//	log(string(message));
//}

const string currentDateTime() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
    // for more information about date/time format
    strftime(buf, sizeof(buf), "%Y-%m-%d %X", &tstruct);

    return buf;
}

void log(string message) {
	cout << currentDateTime() << "\t" << message << endl;
}