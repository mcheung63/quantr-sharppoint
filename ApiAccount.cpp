/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ApiAccount.cpp
 * Author: root
 * 
 * Created on August 11, 2016, 3:32 PM
 */

#include "ApiAccount.h"

ApiAccount::ApiAccount(string ip, string username, string password, string license, string appId) {
	this->ip = ip;
	this->username = username;
	this->password = password;
	this->license = license;
	this->appId = appId;
}

bool ApiAccount::operator<(const ApiAccount& src)const {
	return (this->ip + this->username + this->password + this->license + this->appId) < (src.ip + src.username + src.password + src.license + src.appId);
}
