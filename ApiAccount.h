/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ApiAccount.h
 * Author: root
 *
 * Created on August 11, 2016, 3:32 PM
 */

#ifndef APIACCOUNT_H
#define APIACCOUNT_H

#include <string>
using namespace std;

class ApiAccount {
public:
	string ip;
	string username;
	string password;
	string license;
	string appId;

	ApiAccount(string ip, string username, string password, string license, string appId);

	bool operator<(const ApiAccount& src)const;
};

#endif /* APIACCOUNT_H */

