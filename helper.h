#ifndef HELPER_H
#define HELPER_H

#include <mutex>
#include <map>
#include "ApiTester.h"
#include "ApiAccount.h"
using namespace std;

void myApiTickerUpdate(const ApiTester *apiTester, const SPApiTicker *ticker);
void myApiPriceUpdate(const ApiTester *apiTester, const SPApiPrice *price);

extern mutex loginMutex;
extern map<ApiAccount, ApiTester *> accounts;

#define LOGIN \
	loginMutex.lock();\
	ApiTester *apiTester = accounts[apiAccount];\
	if (apiTester == NULL) {\
		LOGIN_MARCO\
	}\
	apiTester->waitUntilLogin();\
	loginMutex.unlock();

#define LOGIN_MARCO \
	apiTester = new ApiTester();\
	accounts[apiAccount] = apiTester;\
	apiTester->tickerHock=myApiTickerUpdate;\
	apiTester->priceHock=myApiPriceUpdate;\
	strcpy(apiTester->host, ip.c_str());\
	strcpy(apiTester->user_id, username.c_str());\
	strcpy(apiTester->password, password.c_str());\
	apiTester->port = stoi(portstr);\
	strcpy(apiTester->license, license.c_str());\
	if (strlen(appId.c_str())<16){\
		strcpy(apiTester->app_id, appId.c_str());\
	}else{\
		strncpy(apiTester->app_id, appId.c_str(), 16);\
	}\
	int rc = apiTester->LoginSync();\
	if (rc != 1) {\
		loginMutex.unlock();\
		return http_response_builder("login failed", 500).string_response(); \
	}\
	rc = apiTester->AccountLoginSync(apiTester->user_id);\
	if (apiTester->accountLogined != -1 && apiTester->accountLogined != 1){\
		loginMutex.unlock();\
		return http_response_builder("login failed", 500).string_response(); \
	}

void check(string *str, string name, string field, bool *error);
char *outputOrderStatus(char status);
//void log(char *message);
void log(string message);

#endif