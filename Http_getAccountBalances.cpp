class Http_getAccountBalances : public http_resource {

	const http_response render(const http_request& req){
		cout<<"Http_getAccountBalances"<<endl;
		cout.flush();
		string ip = req.get_arg("ip");
		string username = req.get_arg("username");
		string password = req.get_arg("password");
		string license = req.get_arg("license");
		string appId = req.get_arg("appId");
		string portstr = req.get_arg("port");
		string str;
		//		cout << "ip=" << ip << endl;
		//		cout << "username=" << username << endl;
		//		cout << "password=" << password << endl;
		//		cout << "license=" << license << endl;
		//		cout << "appId=" << appId << endl;
		//		cout << "port=" << portstr << endl;
		bool error = FALSE;
		check(&str, "ip", ip, &error);
		check(&str, "username", username, &error);
		check(&str, "password", password, &error);
		check(&str, "license", license, &error);
		check(&str, "appId", appId, &error);
		check(&str, "port", portstr, &error);

		ApiAccount apiAccount(ip, username, password, license, appId);

		if (!error) {
			LOGIN
			//int balCount = apiTester->GetAccBalCountSync((char *)username.c_str());
			vector<SPApiAccBal> apiAccBalList;
			apiTester->GetAllAccBalSync((char *) username.c_str(), &apiAccBalList);
			cout << "apiAccBalList.size()=" << apiAccBalList.size() << endl;
			str = "";
			if (apiAccBalList.size() == 0) {
				str = "0";
			} else {
				str += to_string(apiAccBalList.size()) + "\n";
				for (int i = 0; i < apiAccBalList.size(); i++) {
					SPApiAccBal& accBal = apiAccBalList[i];
					printf("%d Ccy=%s , CashBf=%f\n", i, accBal.Ccy, accBal.CashBf);
					str += to_string(i) + ",Ccy=" + accBal.Ccy + ", CashBf=" + to_string(accBal.CashBf) + ", TodayCash=" + to_string(accBal.TodayCash) + "\n";
				}
			}
		} else {
			return http_response_builder("error, wrong parameter", 500).string_response();
		}
		cout << "------------------------------------------------------------------" << endl;
		return http_response_builder(str, 200).string_response();
	}
};

