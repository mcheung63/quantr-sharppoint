# quantr-sharppoint

Sharppoint connector, part of Quantr open source trading platform project. It is a standalone server to communicate with sharppoint gateway, allow you to get acccount balance, send out order, etc...

These are the design main points:

1. Fully multithread
2. Restful api
3. Every restful api must pass the stress test using JMeter (40 threads x 1000 requests)
4. Handle mult-talent, that mean multiple sharppoint gateways, multiple logins and multiple users

# compile

## compile libmicrohttpd

1. (ubuntu apt-get microhttpd won't work) so : wget http://ftp.gnu.org/gnu/libmicrohttpd/libmicrohttpd-latest.tar.gz
2. tar zxvf libmicrohttpd-latest.tar.gz
3. cd libmicrohttpd-0.9.50
4. ./configure
5. make
6. make install
		
## compile libhttpserver

1. git clone https://github.com/etr/libhttpserver.git
2. cd libhttpserver
3. make -f Makefile.cvs
4. cd ..
5. mkdir build-libhttpserver
6. cd build-libhttpserver
7. ../libhttpserver/configure
8. make
9. make install

## compile cURLpp

1. don't apt-get cURLpp
2. git clone https://github.com/jpbarrette/curlpp.git
3. apt-get install g++-4.8
4. cd curlpp
5. apt-get install libboost-all-dev -y
6. apt-get install libcurl4-gnutls-dev -y
7. apt-get install libtool* -y
8. ./autogen.sh 
9. CXXFLAGS=-std=c++0x CC=gcc-4.8 CXX=g++-4.8 ./configure
10. make
11. make install

## install libcurses-dev

1. apt-get install libncurses5-dev
		
## install mysql c++ connector by apt-get
		
1. apt-get install libmysqlcppconn-dev
		
## install mysql c++ connector by source code

1. warning : don't use binary from mysql.com, compile the source yourself. Binary just crash in ubuntu 14.04 lts.
2. apt-get install libmysqlclient-dev , this is for compiling the source code
3. download it from https://dev.mysql.com/downloads/connector/cpp/
4. cmake .
5. make
6. make install

## create db

1. login to mysql and create database sp, username is sp, password is sp
2. run the createTable.sql
		

## compile
make
		
## run
make run

## debug using gdb
		
1. gdb sp
2. set env LD_LIBRARY_PATH lib/:/usr/local/lib:/root/Downloads/mysql-connector-c++-1.1.7-linux-glibc2.5-x86-64bit/lib
3. run


